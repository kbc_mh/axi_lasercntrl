Für ein reibungsloses Arbeiten mit Vivado und Revision Control wird ein Skript basierter Workflow verwendet.
Dabei sind zu versionierende Daten strikt von den durch die Tools erzeugten Daten zu trennen.
Beim Starten der Tools wird der Projektpfad automatisch auf das Lauftwerk W: gemappt, da die Xilinx-Tools Probleme mit langen Pfaden haben.
Im Root-Verzeichnis befinden sich alle Skripte, die für das Arbeiten mit den Tools notwendig sind. 

"_run_sdk" startet das Xilinx SDK mit dem Workspace w:\work\sdk
"_run_vivado" startet Vivado mit dem entsprechenden Projekt in w:\work\vivado
Wenn kein Projekt existiert, wird dieses aus proj_xxx.tcl und bd_xxx.tcl gebaut.
"_unmap_workdrive" entfernt das Arbeitslaufwerk W: 

"proj_xxx.tcl" erzeugt ein neues Vivado-Projekt. "xxx" ist Platzhalter für die verwendete Version der Xilinx-Tools. Das Template ist ein ninimales Projekt (derzeit für ein Trenz-Elektronik TE820-3EG-2GB-Modul auf dem TE705 Eval-board. 

Neues Projekt aus Template erzeugen
1. Projekt-Orderstruktur an neuen Platz kopieren
2. In "start.tcl" oben bei "set project_name" den Projektnamen eintragen.
3. "_run_vivado" auführen

Geändertes Projekt in tcl-skripte "sichern"
1. In Vivado das Blockdiagramm öffnen und das Blockdesign in "bd_xxx.tcl" exportieren
   (File->Export->Export-Block-Design, "Automatically create top design" deaktivieren).
2. "save_project.tcl"-Skript (im Projekt-Root) ausführen. 
3. Im erzeugten "proj_xxx"-Skript alle Verweise auf das Block-Design (*.bd) löschen. 
   Bei "# Set the project name" Befehlzeile durch "set _xil_proj_name_ $project_name" ersetzen.

Bestehendes Projekt öffnen:
1. "_run_vivado" auführen

SDK öffnen:
1. "_run_SDK" auführen

Work drive entfernen:
1: "_unmap_workdrive" ausführen