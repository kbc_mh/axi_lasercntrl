
#ifndef KBCLASERCNTRL_H
#define KBCLASERCNTRL_H

/****************** Include Files ********************/
#include "xil_types.h"
#include "xstatus.h"
#include "xil_io.h"

/* Register offsets */
#define KBCLASERCNTRL_CR_OFFSET 	0x00000000U //Control
#define KBCLASERCNTRL_CMD_OFFSET 	0x00000004U //Command
#define KBCLASERCNTRL_PS_OFFSET 	0x0000000CU //Power Scale (not implemented yet)
#define KBCLASERCNTRL_SPL_OFFSET 	0x00000010U //Standby Pulse Length
#define KBCLASERCNTRL_SPP_OFFSET 	0x00000014U //Standby Pulse Period
#define KBCLASERCNTRL_ONPL_OFFSET 	0x00000018U //On Pulse Length
#define KBCLASERCNTRL_ONPP_OFFSET 	0x0000001CU //On Pulse Period
#define KBCLASERCNTRL_ONPD_OFFSET 	0x00000020U //On Pulse Delay (not implemented yet)
#define KBCLASERCNTRL_FPKPL_OFFSET 	0x00000024U //First Pulse Killer Pulse Length (not implemented yet)
#define KBCLASERCNTRL_ADATA_OFFSET 	0x00000030U //Analog Out Data
#define KBCLASERCNTRL_DDATA_OFFSET 	0x00000034U //Digital Out Data
#define KBCLASERCNTRL_SR_OFFSET 	0x0000003CU //Status (read only)

/* Control reg masks*/
#define KBCLASERCNTRL_LASMODE_MASK 	0x0000000FU //Laser mode
#define KBCLASERCNTRL_LASEN_MASK 	0x00000010U //Laser enable
#define KBCLASERCNTRL_SPALGN_MASK 	0x00000020U //Standby Pulse alignment
#define KBCLASERCNTRL_LASSRC_MASK 	0x00000040U //Laser Data Source
#define KBCLASERCNTRL_ANMODE_MASK 	0x00000300U //Analog Out Mode
#define KBCLASERCNTRL_AN0SRC_MASK 	0x00000C00U //Analog Out 0 Data Source
#define KBCLASERCNTRL_AN1SRC_MASK 	0x00003000U //Analog Out 1 Data Source
#define KBCLASERCNTRL_DIGSRC_MASK 	0x0000C000U //Digital Out Data Source

/* Control reg offsets*/
#define KBCLASERCNTRL_LASMODE_SHIFT 0U //Laser mode
#define KBCLASERCNTRL_LASEN_SHIFT 	4U //Laser enable
#define KBCLASERCNTRL_SPALGN_SHIFT 	5U //Standby Pulse alignment
#define KBCLASERCNTRL_LASSRC_SHIFT 	6U //Laser Data Source
#define KBCLASERCNTRL_ANMODE_SHIFT 	16U //Analog Out Config
#define KBCLASERCNTRL_AN0SRC_SHIFT 	18U //Analog Out 0 Data Source
#define KBCLASERCNTRL_AN1SRC_SHIFT 	20U //Analog Out 1 Data Source
#define KBCLASERCNTRL_DIGSRC_SHIFT 	22U //Digital Out Data Source

/* Command reg masks */
#define KBCLASERCNTRL_DELAY_MASK 	0x00FFFFFFU //Laser (on/off) Delay
#define KBCLASERCNTRL_PWRSTAT_MASK  0x10000000U //Power State

/* Command reg offsets */
#define KBCLASERCNTRL_DELAY_SHIFT 	0U //Laser (on/off) Delay
#define KBCLASERCNTRL_PWRSTAT_SHIFT 31U //Power State

/* Status reg masks */
#define KBCLASERCNTRL_STATLASEN_MASK 	0x00000001U //Laser enabled
#define KBCLASERCNTRL_STATLASFIRE_MASK 	0x00000002U //Laser firing
#define KBCLASERCNTRL_STATLASFO_MASK 	0x00000004U //Laser forced off

/* Status reg offsets */
#define KBCLASERCNTRL_STATLASEN_SHIFT 	0U //Laser enabled
#define KBCLASERCNTRL_STATLASFIRE_SHIFT 1U //Laser firing
#define KBCLASERCNTRL_STATLASFO_SHIFT 	2U //Laser forced off

#define KBCLASERCNTRL_PULSEGEN_FREQ	100e6 //Pulse generator tick frequency

/**************************** Type Definitions *****************************/

/**
 * This typedef contains configuration information for the device.
 */
typedef struct {
	u16 DeviceId;		/**< Unique ID of device */
	u32 BaseAddress;		/**< Register base address */
} KBCLaserCntrl_Config;

/**
 * The KBCLaserCntrl driver instance data. The user is required to allocate a
 * variable of this type for every KBCScanOut device in the system. A pointer
 * to a variable of this type is then passed to the driver API functions.
 */
typedef struct {
	UINTPTR BaseAddress;	/* Device base address */
	u32 IsReady;			/* Device is initialized and ready */
} KBCLaserCntrl;

typedef enum {LasSrcPl, LasSrcAxi} KBCLaserCntrl_LasSrc;

typedef enum {LasModeDis, LasModeCO2} KBCLaserCntrl_LasMode;

typedef enum {LasAuxSrcDis, LasAuxSrcPwr, LasAuxSrcAxi, LasAuxSrcPl} KBCLaserCntrl_LasAuxSrc;

typedef enum {LasAnOutIndep, LasAnOutNDef0, LasAnOutSync, LasAnOutNDef1} KBCLaserCntrl_AnOutMode;

/**
 *
 * Write a value to a KBCLaserCntrl register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the AXI_LASERCNTRLdevice.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void KBCLaserCntrl_mWriteReg(u32 BaseAddress, unsigned RegOffset, u32 Data)
 *
 */
#define KBCLaserCntrl_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/**
 *
 * Read a value from a KBCLaserCntrl register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the AXI_LASERCNTRL device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	u32 KBCLaserCntrl_mReadReg(u32 BaseAddress, unsigned RegOffset)
 *
 */
#define KBCLaserCntrl_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the KBCLaserCntrl instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus KBCLaserCntrl_Reg_SelfTest(void * baseaddr_p);

/* Device Initialization functions*/
KBCLaserCntrl_Config *KBCLaserCntrl_LookupConfig(u16 DeviceId);
int KBCLaserCntrl_CfgInitialize(KBCLaserCntrl* pInst, KBCLaserCntrl_Config* pConfig,
			UINTPTR EffectiveAddr);
int KBCLaserCntrl_Initialize(KBCLaserCntrl* pInst, u16 DeviceId);

/* Laser signal generator API functions */
void KBCLaserCntrl_SetLaserSrc(KBCLaserCntrl *pInst, KBCLaserCntrl_LasSrc Source);
void KBCLaserCntrl_SetLaserMode(KBCLaserCntrl *pInst, KBCLaserCntrl_LasMode Mode);
void KBCLaserCntrl_SetLaserEnable(KBCLaserCntrl *pInst, u32 Enable);
void KBCLaserCntrl_SetStdbyAlign(KBCLaserCntrl *pInst, u32 Enable);
void KBCLaserCntrl_SetStdbyPulse(KBCLaserCntrl *pInst, float Length, float Period);
void KBCLaserCntrl_SetOnPulse(KBCLaserCntrl *pInst, float Length, float Period);
void KBCLaserCntrl_SetOnPulsePeriod(KBCLaserCntrl *pInst, float Period);
void KBCLaserCntrl_SetOnPulseLength(KBCLaserCntrl *pInst, float Length);
void KBCLaserCntrl_SetLaserState(KBCLaserCntrl *pInst, u32 State, float Delay);

/* Analog (DAC) output API functions */
void KBCLaserCntrl_SetAnalogSrc(KBCLaserCntrl *pInst, u32 Ch, KBCLaserCntrl_LasAuxSrc Source);
void KBCLaserCntrl_SetAnalogMode(KBCLaserCntrl *pInst, KBCLaserCntrl_AnOutMode Mode);
void KBCLaserCntrl_WriteAnalogData(KBCLaserCntrl *pInst, u32 Ch, u32 Data, u32 Update);

/* Digital output API functions */
void KBCLaserCntrl_SetDigitalSrc(KBCLaserCntrl *pInst, KBCLaserCntrl_LasSrc Source);
void KBCLaserCntrl_WriteDigitalData(KBCLaserCntrl *pInst, u32 Data);

#endif // KBCLASERCNTRL_H
