/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

/***************************** Include Files *********************************/
#include <stdio.h>
#include "platform.h"
#include "xil_types.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "kbcLaserCntrl.h"
#include "xttcps.h"
#include "xscugic.h"
#include "xgpio.h"

/************************** Constant Definitions *****************************/
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are only defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define TTC_TICK_DEVICE_ID	XPAR_XTTCPS_0_DEVICE_ID
#define TTC_TICK_INTR_ID	XPAR_XTTCPS_0_INTR

#define LASERCNTRL_DEVICE_ID 	XPAR_AXI_LASERCNTRL_0_DEVICE_ID

#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID

#define AXI_GPIO0_DEVICE_ID XPAR_GPIO_0_DEVICE_ID

/**************************** Type Definitions *******************************/
typedef struct {
	u32 OutputHz;	/* Output frequency */
	XInterval Interval;	/* Interval value */
	u8 Prescaler;	/* Prescaler value */
	u16 Options;	/* Option settings */
} TmrCntrSetup;

/************************** Function Prototypes ******************************/
static int SetupTimer(int DeviceID);
static int SetupTicker(void);
static int SetupInterruptSystem(u16 IntcDeviceID, XScuGic *IntcInstancePtr);
static void TickHandler(void *CallBackRef);
void PrintAssert(const char8 *File, s32 Line);

/************************** Variable Definitions *****************************/
static KBCLaserCntrl gLaserCntrl;
static XTtcPs gTtcPsInst[2];
static XGpio gGpio; /* The Instance of the GPIO Driver */
XScuGic gIntc;  /* Interrupt controller instance */

static TmrCntrSetup SettingsTable[2] = {
	{3, 0, 0, 0},	/* Ticker timer counter initial setup, only output freq */
	{200, 0, 0, 0}, /* PWM timer counter initial setup, only output freq */
};

int main()
{
	int Status;

	init_platform();
	Xil_AssertSetCallback(&PrintAssert);

    print("Hello World\n\r");

    /* Initialize the GPIO driver */
	Status = XGpio_Initialize(&gGpio, AXI_GPIO0_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Setup AxiGpio0 failed\n");
	}
	else {
		xil_printf("Setup AxiGpio0 success\n");
	}

	/* Set the direction for all signals as outputs */
	XGpio_SetDataDirection(&gGpio, 1, 0);

    /*
	 * Connect the Intc to the interrupt subsystem such that interrupts can
	 * occur. This function is application specific.
	 */
	Status = SetupInterruptSystem(INTC_DEVICE_ID, &gIntc);
	if (Status != XST_SUCCESS) {
		xil_printf("Setup Interrupt failed\n");
	}
	else {
		xil_printf("Setup Interrupt success\n");
	}


	XGpio_DiscreteWrite(&gGpio, 1, 1000); //Set PL-Divider

	Status = KBCLaserCntrl_Initialize(&gLaserCntrl, XPAR_AXI_LASERCNTRL_0_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Setup Laser Control failed\n");
	}
	else {
		xil_printf("Setup Laser Control success\n");
	}

    /*
	 * Set up the Ticker timer
	 */
	Status = SetupTicker();
	if (Status != XST_SUCCESS) {
		xil_printf("Setup Timer failed\n");
	}
	else {
		xil_printf("Setup Timer success\n");
	}

	KBCLaserCntrl_SetLaserSrc(&gLaserCntrl, LasSrcAxi);
	KBCLaserCntrl_SetLaserMode(&gLaserCntrl, LasModeCO2);
	KBCLaserCntrl_SetStdbyAlign(&gLaserCntrl, 1);
	KBCLaserCntrl_SetStdbyPulse(&gLaserCntrl, 2.0F, 200.0F);
	KBCLaserCntrl_SetOnPulse(&gLaserCntrl, 1.33F, 6.0F);
	KBCLaserCntrl_SetLaserEnable(&gLaserCntrl, 1);

	KBCLaserCntrl_SetAnalogSrc(&gLaserCntrl, 0, LasAuxSrcAxi);
	KBCLaserCntrl_SetAnalogSrc(&gLaserCntrl, 1, LasAuxSrcPl);
	KBCLaserCntrl_SetAnalogMode(&gLaserCntrl, LasAnOutIndep);


    while(1)
    {

    }

    cleanup_platform();
    return 0;
}


static void TickHandler(void *CallBackRef)
{
	u32 StatusEvent;
	static u32 count=0;
	static u32 level=0;
	static u32 divCnt=0;

	XTtcPs *Timer = (XTtcPs *)CallBackRef;


	//xil_printf("level: %d\n",level);
	/*
	 * Read the interrupt status, then write it back to clear the interrupt.
	 */
	StatusEvent = XTtcPs_GetInterruptStatus(Timer);
	XTtcPs_ClearInterruptStatus(Timer, StatusEvent);

	KBCLaserCntrl_WriteAnalogData(&gLaserCntrl, 0, level, 0);
			level = (level + 1) & 0xFFFF;

	if (0 != (XTTCPS_IXR_INTERVAL_MASK & StatusEvent)) {
#if 0
		KBCLaserCntrl_SetLaserState(&gLaserCntrl, level, count);
		if(level == 0)
		{
			if(divCnt++ == 2)
			{
				level = 1;
				divCnt = 0;
				count = (count+1) & 0x1F;
			}

		}
		else
		{
			level = 0;
		}
#endif

	}
}

/****************************************************************************/
/**
*
* This function setups the interrupt system such that interrupts can occur.
* This function is application specific since the actual system may or may not
* have an interrupt controller.  The TTC could be directly connected to a
* processor without an interrupt controller.  The user should modify this
* function to fit the application.
*
* @param	IntcDeviceID is the unique ID of the interrupt controller
* @param	IntcInstacePtr is a pointer to the interrupt controller
*		instance.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note		None.
*
*****************************************************************************/
static int SetupInterruptSystem(u16 IntcDeviceID,
				    XScuGic *IntcInstancePtr)
{
	int Status;
	XScuGic_Config *IntcConfig; /* The configuration parameters of the
					interrupt controller */

	/*
	 * Initialize the interrupt controller driver
	 */
	IntcConfig = XScuGic_LookupConfig(IntcDeviceID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the ARM processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			IntcInstancePtr);

	/*
	 * Enable interrupts in the ARM
	 */
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}

/****************************************************************************/
/**
*
* This function sets up a timer counter device, using the information in its
* setup structure.
*  . initialize device
*  . set options
*  . set interval and prescaler value for given output frequency.
*
* @param	DeviceID is the unique ID for the device.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note		None.
*
*****************************************************************************/
int SetupTimer(int DeviceID)
{
	int Status;
	XTtcPs_Config *Config;
	XTtcPs *Timer;
	TmrCntrSetup *TimerSetup;

	TimerSetup = &SettingsTable[DeviceID];

	Timer = &(gTtcPsInst[DeviceID]);

	/*
	 * Look up the configuration based on the device identifier
	 */
	Config = XTtcPs_LookupConfig(DeviceID);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	/*
	 * Initialize the device
	 */
	Status = XTtcPs_CfgInitialize(Timer, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		if (Status == XST_DEVICE_IS_STARTED) {
			XTtcPs_Stop(Timer);
			Status = XTtcPs_CfgInitialize(Timer, Config, Config->BaseAddress);
		}
	}
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the options
	 */
	XTtcPs_SetOptions(Timer, TimerSetup->Options);

	/*
	 * Timer frequency is preset in the TimerSetup structure,
	 * however, the value is not reflected in its other fields, such as
	 * IntervalValue and PrescalerValue. The following call will map the
	 * frequency to the interval and prescaler values.
	 */
	XTtcPs_CalcIntervalFromFreq(Timer, TimerSetup->OutputHz,
		&(TimerSetup->Interval), &(TimerSetup->Prescaler));

	/*
	 * Set the interval and prescale
	 */
	XTtcPs_SetInterval(Timer, TimerSetup->Interval);
	XTtcPs_SetPrescaler(Timer, TimerSetup->Prescaler);

	return XST_SUCCESS;
}

/****************************************************************************/
/**
*
* This function sets up the Ticker timer.
*
* @param	None
*
* @return	XST_SUCCESS if everything sets up well, XST_FAILURE otherwise.
*
* @note		None
*
*****************************************************************************/
int SetupTicker(void)
{
	int Status;
	TmrCntrSetup *TimerSetup;
	XTtcPs *TtcPsTick;


	TimerSetup = &(SettingsTable[TTC_TICK_DEVICE_ID]);

	/*
	 * Set up appropriate options for Ticker: interval mode without
	 * waveform output.
	 */
	TimerSetup->Options |= (XTTCPS_OPTION_INTERVAL_MODE |
					      XTTCPS_OPTION_WAVE_DISABLE);

	/*
	 * Calling the timer setup routine
	 *  . initialize device
	 *  . set options
	 */
	Status = SetupTimer(TTC_TICK_DEVICE_ID);
	if(Status != XST_SUCCESS) {
		return Status;
	}

	TtcPsTick = &(gTtcPsInst[TTC_TICK_DEVICE_ID]);

	/*
	 * Connect to the interrupt controller
	 */
	Status = XScuGic_Connect(&gIntc, TTC_TICK_INTR_ID,
		(Xil_ExceptionHandler)TickHandler, (void *)TtcPsTick);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Enable the interrupt for the Timer counter
	 */
	XScuGic_Enable(&gIntc, TTC_TICK_INTR_ID);

	/*
	 * Enable the interrupts for the tick timer/counter
	 * We only care about the interval timeout.
	 */
	XTtcPs_EnableInterrupts(TtcPsTick, XTTCPS_IXR_INTERVAL_MASK);

	/*
	 * Start the tick timer/counter
	 */
	XTtcPs_Start(TtcPsTick);

	return Status;
}

void PrintAssert(const char8 *File, s32 Line)
{
	xil_printf("Assert in File %s in Line %d\n",File,Line);
}
