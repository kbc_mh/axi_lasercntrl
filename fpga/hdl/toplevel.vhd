----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.03.2019 11:04:56
-- Design Name: 
-- Module Name: toplevel - structure
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity toplevel is
  port (
    -------------------------------------------------------------------------------------------
    -- processor system
    -------------------------------------------------------------------------------------------

    -- LEDs            
    Led1_N                                      : out   std_logic;
    Led2_N                                      : out   std_logic;
    Led3_N                                      : out   std_logic;

    I2C_SCL_PL                                  : inout std_logic;
    I2C_SDA_PL                                  : inout std_logic;
    
    -- MIG                
--    DDR4PL_ACT_n                                : out   std_logic;
--    DDR4PL_A                                    : out   std_logic_vector ( 16 downto 0 );
--    DDR4PL_BA                                   : out   std_logic_vector ( 1 downto 0 );
----    DDR4PL_BG                                   : out   std_logic_vector ( 1 downto 0 );
--    DDR4PL_BG                                   : out   std_logic_vector ( 0 to 0 );
--    DDR4PL_CK_P                                 : out   std_logic_vector ( 0 to 0 );
--    DDR4PL_CK_N                                 : out   std_logic_vector ( 0 to 0 );
--    DDR4PL_CKE                                  : out   std_logic_vector ( 0 to 0 );
--    DDR4PL_CS_n                                 : out   std_logic_vector ( 0 to 0 );
--    DDR4PL_DM                                   : inout std_logic_vector ( 1 downto 0 );
--    DDR4PL_DQ                                   : inout std_logic_vector ( 15 downto 0 );
--    DDR4PL_DQS_N                                : inout std_logic_vector ( 1 downto 0 );
--    DDR4PL_DQS_P                                : inout std_logic_vector ( 1 downto 0 );
--    DDR4PL_ODT                                  : out   std_logic_vector ( 0 to 0 );
--    DDR4PL_RST_n_PL                             : out   std_logic;
                
    CLK100_PL_N                                 : in    std_logic;
    CLK100_PL_P                                 : in    std_logic;

    ---------------------------------------------------------------------------------------------------
    -- I/Os available only on the XU5-2EG/3EG variants and on the XU5-4EG/5EG-G1 variants
    --    bank 65 & 66
    --    bank 26 (ZU2/3) or 46 (ZU4/5)
    ---------------------------------------------------------------------------------------------------
    IO_B65_L3_AD15_U8_P                         : inout std_logic;
    IO_B65_L3_AD15_V8_N                         : inout std_logic;
    IO_B65_L4_AD7_ALERT_R8_P                    : inout std_logic;
    IO_B65_L4_AD7_T8_N                          : inout std_logic;
    IO_B65_L5_AD14_R7_P                         : inout std_logic;
    IO_B65_L5_AD14_T7_N                         : inout std_logic;
    IO_B65_L11_GC_K3_N                          : inout std_logic;
    IO_B65_L11_GC_K4_P                          : inout std_logic;
    IO_B66_L1_F1_N                              : inout std_logic;
    IO_B66_L1_G1_P                              : inout std_logic;
    IO_B66_L2_D1_N                              : inout std_logic;
    IO_B66_L2_E1_P                              : inout std_logic;
    IO_B66_L3_AD15_E2_N                         : inout std_logic;
    IO_B66_L3_AD15_F2_P                         : inout std_logic;
    IO_B66_L4_AD7_F3_N                          : inout std_logic;
    IO_B66_L4_AD7_G3_P                          : inout std_logic;
    IO_B66_L5_AD14_E3_N                         : inout std_logic;
    IO_B66_L5_AD14_E4_P                         : inout std_logic;
    IO_B66_L11_GC_C4_N                          : inout std_logic;
    IO_B66_L11_GC_D4_P                          : inout std_logic;
    IO_BF_L8_HDGC_AD4_E15_N                     : inout std_logic;
    IO_BF_L8_HDGC_AD4_F15_P                     : inout std_logic; 

    ---------------------------------------------------------------------------------------------------
    -- bank 65
    ---------------------------------------------------------------------------------------------------
    IO_B65_L1_W8                                : inout std_logic;
    IO_B65_L1_Y8                                : inout std_logic;
    IO_B65_L10_AD4_H3_N                         : inout std_logic;
    IO_B65_L10_AD4_H4_P                         : inout std_logic;
    IO_B65_L12_GC_L2_N                          : inout std_logic;
    IO_B65_L12_GC_L3_P                          : inout std_logic;
    IO_B65_L13_GC_L6_N                          : inout std_logic;
    IO_B65_L13_GC_L7_P                          : inout std_logic;
    IO_B65_L14_GC_L5_N                          : inout std_logic;
    IO_B65_L14_GC_M6_P                          : inout std_logic;
    IO_B65_L15_AD11_N6_N                        : inout std_logic;
    IO_B65_L15_AD11_N7_P                        : inout std_logic;
    IO_B65_L16_AD3_P6_N                         : inout std_logic;
    IO_B65_L16_AD3_P7_P                         : inout std_logic;
    IO_B65_L17_AD10_N8_N                        : inout std_logic;
    IO_B65_L17_AD10_N9_P                        : inout std_logic;
    IO_B65_L18_AD2_L8_N                         : inout std_logic;
    IO_B65_L18_AD2_M8_P                         : inout std_logic;
    IO_B65_L19_AD9_J4_N                         : inout std_logic;
    IO_B65_L19_AD9_J5_P                         : inout std_logic;
    IO_B65_L2_U9                                : inout std_logic;
    IO_B65_L2_V9                                : inout std_logic;
    IO_B65_L20_AD1_H6_N                         : inout std_logic;
    IO_B65_L20_AD1_J6_P                         : inout std_logic;
    IO_B65_L21_AD8_H7_N                         : inout std_logic;
    IO_B65_L21_AD8_J7_P                         : inout std_logic;
    IO_B65_L22_AD0_K7_N                         : inout std_logic;
    IO_B65_L22_AD0_K8_P                         : inout std_logic;
    IO_B65_L23_J9_N                             : inout std_logic;
    IO_B65_L23_SCLK_K9_P                        : inout std_logic;
    IO_B65_L24_PERSTN_H8_N                      : inout std_logic;
    IO_B65_L24_SDA_H9_P                         : inout std_logic;
    IO_B65_L6_AD6_R6_P                          : inout std_logic;
    IO_B65_L6_AD6_T6_N                          : inout std_logic;
    IO_B65_L7_AD13_K1_N                         : inout std_logic;
    IO_B65_L7_AD13_L1_P                         : inout std_logic;
    IO_B65_L8_AD5_H1_N                          : inout std_logic;
    IO_B65_L8_AD5_J1_P                          : inout std_logic;
    IO_B65_L9_AD12_J2_N                         : inout std_logic;
    IO_B65_L9_AD12_K2_P                         : inout std_logic;    

    ---------------------------------------------------------------------------------------------------
    -- bank 66
    ---------------------------------------------------------------------------------------------------    
    IO_B66_L10_AD4_A4_N                           : inout std_logic;
    IO_B66_L10_AD4_B4_P                           : inout std_logic;
    IO_B66_L12_GC_C2_N                            : inout std_logic;
    IO_B66_L12_GC_C3_P                            : inout std_logic;
    IO_B66_L13_GC_D6_N                            : inout std_logic;
    IO_B66_L13_GC_D7_P                            : inout std_logic;
    IO_B66_L14_GC_D5_N                            : inout std_logic;
    IO_B66_L14_GC_E5_P                            : inout std_logic;
    IO_B66_L15_AD11_F6_N                          : inout std_logic;
    IO_B66_L15_AD11_G6_P                          : inout std_logic;
    IO_B66_L16_AD3_F7_N                           : inout std_logic;
    IO_B66_L16_AD3_G8_P                           : inout std_logic;
    IO_B66_L17_AD10_E8_N                          : inout std_logic;
    IO_B66_L17_AD10_F8_P                          : inout std_logic;
    IO_B66_L18_AD2_D9_N                           : inout std_logic;
    IO_B66_L18_AD2_E9_P                           : inout std_logic;
    IO_B66_L19_AD9_A5_N                           : inout std_logic;
    IO_B66_L19_AD9_B5_P                           : inout std_logic;
    IO_B66_L20_AD1_B6_N                           : inout std_logic;
    IO_B66_L20_AD1_C6_P                           : inout std_logic;
    IO_B66_L21_AD8_A6_N                           : inout std_logic;
    IO_B66_L21_AD8_A7_P                           : inout std_logic;
    IO_B66_L22_AD0_B8_N                           : inout std_logic;
    IO_B66_L22_AD0_C8_P                           : inout std_logic;
    IO_B66_L23_A8_N                               : inout std_logic;
    IO_B66_L23_A9_P                               : inout std_logic;
    IO_B66_L24_B9_N                               : inout std_logic;
    IO_B66_L24_C9_P                               : inout std_logic;
    IO_B66_L6_AD6_F5_N                            : inout std_logic;
    IO_B66_L6_AD6_G5_P                            : inout std_logic;
    IO_B66_L7_AD13_B1_N                           : inout std_logic;
    IO_B66_L7_AD13_C1_P                           : inout std_logic;
    IO_B66_L8_AD5_A1_N                            : inout std_logic;
    IO_B66_L8_AD5_A2_P                            : inout std_logic;
    IO_B66_L9_AD12_A3_N                           : inout std_logic;
    IO_B66_L9_AD12_B3_P                           : inout std_logic;

    ---------------------------------------------------------------------------------------------------
    -- bank N - 24 (ZU2/3) or 44 (ZU4/5)
    ---------------------------------------------------------------------------------------------------
    IO_BN_L1_AD15_AE14_N                        : inout std_logic;
    IO_BN_L1_AD15_AE15_P                        : inout std_logic;
    IO_BN_L10_AD10_Y13_N                        : inout std_logic;
    IO_BN_L10_AD10_Y14_P                        : inout std_logic;
    IO_BN_L11_AD9_W11_N                         : inout std_logic;
    IO_BN_L11_AD9_W12_P                         : inout std_logic;
    IO_BN_L12_AD8_AA12_N                        : inout std_logic;
    IO_BN_L12_AD8_Y12_P                         : inout std_logic;
    IO_BN_L2_AD14_AG14_P                        : inout std_logic;
    IO_BN_L2_AD14_AH14_N                        : inout std_logic;
    IO_BN_L3_AD13_AG13_P                        : inout std_logic;
    IO_BN_L3_AD13_AH13_N                        : inout std_logic;
    IO_BN_L4_AD12_AE13_P                        : inout std_logic;
    IO_BN_L4_AD12_AF13_N                        : inout std_logic;
    IO_BN_L5_HDGC_AD14_N                        : inout std_logic;
    IO_BN_L5_HDGC_AD15_P                        : inout std_logic;
    IO_BN_L6_HDGC_AC13_N                        : inout std_logic;
    IO_BN_L6_HDGC_AC14_P                        : inout std_logic;
    IO_BN_L7_HDGC_AA13_P                        : inout std_logic;
    IO_BN_L7_HDGC_AB13_N                        : inout std_logic;
    IO_BN_L8_HDGC_AB14_N                        : inout std_logic;
    IO_BN_L8_HDGC_AB15_P                        : inout std_logic;
    IO_BN_L9_AD11_W13_N                         : inout std_logic;
    IO_BN_L9_AD11_W14_P                         : inout std_logic;

    ---------------------------------------------------------------------------------------------------
    -- bank O - 44 (ZU2/3) or 43 (ZU4/5)
    ---------------------------------------------------------------------------------------------------
    IO_BO_L1_AD11_AG10_P                        : inout std_logic;
    IO_BO_L1_AD11_AH10_N                        : inout std_logic;
    IO_BO_L10_AD2_W10_P                         : inout std_logic;
    IO_BO_L10_AD2_Y10_N                         : inout std_logic;
    IO_BO_L11_AD1_AA8_N                         : inout std_logic;
    IO_BO_L11_AD1_Y9_P                          : inout std_logic;
    IO_BO_L12_AD0_AB10_P                        : inout std_logic;
    IO_BO_L12_AD0_AB9_N                         : inout std_logic;
    IO_BO_L2_AD10_AF11_P                        : inout std_logic;
    IO_BO_L2_AD10_AG11_N                        : inout std_logic;
    IO_BO_L3_AD9_AH11_N                         : inout std_logic;
    IO_BO_L3_AD9_AH12_P                         : inout std_logic;
    IO_BO_L4_AD8_AE10_P                         : inout std_logic;
    IO_BO_L4_AD8_AF10_N                         : inout std_logic;
    IO_BO_L5_HDGC_AD7_AE12_P                    : inout std_logic;
    IO_BO_L5_HDGC_AD7_AF12_N                    : inout std_logic;
    IO_BO_L6_HDGC_AD6_AC12_P                    : inout std_logic;
    IO_BO_L6_HDGC_AD6_AD12_N                    : inout std_logic;
    IO_BO_L7_HDGC_AD5_AD10_N                    : inout std_logic;
    IO_BO_L7_HDGC_AD5_AD11_P                    : inout std_logic;
    IO_BO_L8_HDGC_AD4_AB11_P                    : inout std_logic;
    IO_BO_L8_HDGC_AD4_AC11_N                    : inout std_logic;
    IO_BO_L9_AD3_AA10_N                         : inout std_logic;
    IO_BO_L9_AD3_AA11_P                         : inout std_logic;

    ---------------------------------------------------------------------------------------------------
    -- bank 25 (ZU2/3) or 45 (ZU4/5)
    ---------------------------------------------------------------------------------------------------    

    IO_BE_L8_HDGC_D11_N                         : inout std_logic;
    IO_BE_L8_HDGC_E12_P                         : inout std_logic;
    IO_BE_L7_HDGC_D10_N                         : inout std_logic;
    IO_BE_L7_HDGC_E10_P                         : inout std_logic;   
    
    ---------------------------------------------------------------------------------------------------
    -- Gigabit Ethernet interface 1 on PL side
    -- bank 25 (ZU2/3) or 45 (ZU4/5)
    ---------------------------------------------------------------------------------------------------
    ETH1_RXD                                : in    std_logic_vector ( 3 downto 0 );
    ETH1_RXCLK                              : in    std_logic;
    ETH1_RXCTL                              : in    std_logic;
    ETH1_TXD                                : out   std_logic_vector ( 3 downto 0 );
    ETH1_TXCLK                              : out   std_logic;
    ETH1_TXCTL                              : out   std_logic;
    ETH1_INT_n                              : in    std_logic;
    ETH1_RESET_n                            : out   std_logic;
    ETH1_MDC                                : out   std_logic;
    ETH1_MDIO                               : inout std_logic

  );
end toplevel;


architecture structure of toplevel is

 component system is
  port (
    axiGpio_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    debug_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    fclk100mhz_o : out STD_LOGIC;
    fclk200mhz_o : out STD_LOGIC;
    srst100mhz_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    traceClk_o : out STD_LOGIC;
    traceCtl_o : out STD_LOGIC;
    tracedata_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    laser2_o : out STD_LOGIC;
    laser1_o : out STD_LOGIC;
    laserOn_o : out STD_LOGIC;
    dacSync_o : out STD_LOGIC;
    dacSdata_o : out STD_LOGIC;
    dacSclk_o : out STD_LOGIC;
    GPIO_tri_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    GPIO_tri_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    GPIO_tri_t : out STD_LOGIC_VECTOR ( 15 downto 0 );
    an0Data_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    an1Data_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    digData_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    an0Strb_i : in STD_LOGIC;
    an1Strb_i : in STD_LOGIC;
    digStrb_i : in STD_LOGIC;
    forceOff_i : in STD_LOGIC
  );
  end component system;
  

  signal s_gpio_in : std_logic_vector(15 downto 0);
  signal s_gpio_out : std_logic_vector(15 downto 0);
  signal s_gpio_tri : std_logic_vector(15 downto 0);

  signal s_axiGpio : std_logic_vector(31 downto 0);

  signal s_SysDebug : std_logic_vector(15 downto 0);
  
  signal s_fclk100 : std_logic;
  signal s_fclk200 : std_logic;
  signal s_srst100 : std_logic;
  
  signal s_traceClk : std_logic;
  signal s_traceCtl : std_logic;
  signal s_traceData : std_logic_vector(15 downto 0);

  signal s_dacCs : std_logic;
  signal s_dacSclk : std_logic;
  signal s_dacSdata : std_logic;
  signal s_laser1 : std_logic;
  signal s_laser2 : std_logic;
  signal s_laserOn : std_logic;
  
  signal s_pwrStrb : std_logic;
  signal s_data : unsigned(15 downto 0);
  signal s_pwrData : std_logic_vector(15 downto 0);
  
  signal s_divCnt_r : unsigned (16 downto 0);
  signal s_cntThrshld_r : unsigned (9 downto 0);
  signal s_dataCnt_r : unsigned (15 downto 0);

  signal s_forceOff : std_logic;
  signal s_an0Data : std_logic_vector(15 downto 0);
  signal s_an0Strb : std_logic;
  signal s_an1Data : std_logic_vector(15 downto 0);
  signal s_an1Strb : std_logic;
  signal s_digData : std_logic_vector(7 downto 0);
  signal s_digStrb : std_logic;

begin

system_i : component system
  port map (
    forceOff_i => s_forceOff,
    an0Data_i => s_an0Data,
    an0Strb_i => s_an0Strb,
    an1Data_i => s_an1Data,
    an1Strb_i => s_an1Strb,
    digData_i => s_digData,
    digStrb_i => s_digStrb,
    GPIO_tri_i => s_gpio_in,    
    GPIO_tri_o => s_gpio_out,      
    GPIO_tri_t => s_gpio_tri,
    axiGpio_o => s_axiGpio,  
    dacSync_o => s_dacCs,
    dacSclk_o => s_dacSclk,
    dacSdata_o => s_dacSdata,
    laser1_o => s_laser1,
    laser2_o => s_laser2,
    laserOn_o => s_laserOn,    
    debug_o => s_sysDebug,
    fclk100mhz_o => s_fclk100,
    fclk200mhz_o => s_fclk200,
    srst100mhz_o(0) => s_srst100,
    traceClk_o => s_traceClk,
    traceCtl_o => s_traceCtl,
    tracedata_o => s_tracedata
  );
    
  process(s_fclk100)
  begin 
    if(rising_edge(s_fclk100)) then
      if(s_srst100 = '1') then
        s_divCnt_r <= (others => '0');
        s_an0Strb <= '0';
        
        s_dataCnt_r <= (others => '0');
      else
        s_an0Strb <= '0';
        if(s_divCnt_r >= unsigned(s_axiGpio(9 downto 0))) then                    
          s_an0Strb <= '1';   
          s_divCnt_r <= (others => '0');       
          s_dataCnt_r <= s_dataCnt_r + 1;       
        else   
          s_divCnt_r <= s_divCnt_r + 1;               
        end if;
      end if;
    end if;
  end process; 
  
  s_an1Strb <= s_an0Strb;
  s_digStrb <= s_an0Strb; 
   
  s_an0Data <= std_logic_vector(s_dataCnt_r);
  s_an1Data <= std_logic_vector(s_dataCnt_r);
  s_digData <= std_logic_vector(s_dataCnt_r(15 downto 8));
 
  IO_B66_L4_AD7_F3_N <= s_dacSclk;   --IOD0
  IO_B66_L4_AD7_G3_P <= s_dacCs;   --IOD1
  IO_B65_L4_AD7_ALERT_R8_P <= s_dacSdata; --IOD2
  IO_B65_L4_AD7_T8_N <= s_laser1; --IOD3
  IO_B66_L5_AD14_E4_P <= s_laser2; --IOD4
  IO_B66_L5_AD14_E3_N <= s_laserOn; --IOD5 
  IO_B65_L5_AD14_R7_P <= '0'; --IOD6
  IO_B65_L5_AD14_T7_N <= s_an0Strb; --IOD7
      
  IO_B66_L2_E1_P <= s_sysDebug(0); --IOC0
  IO_B66_L2_D1_N <= s_sysDebug(1); --IOC1
  IO_B65_L11_GC_K4_P <= s_sysDebug(2); --IOC2
  IO_B65_L11_GC_K3_N <= s_sysDebug(3); --IOC3
  IO_B66_L3_AD15_F2_P <= s_sysDebug(4); --IOC4
  IO_B66_L3_AD15_E2_N <= s_sysDebug(5); --IOC5
  IO_B65_L3_AD15_U8_P <= s_sysDebug(12); --IOC6
  IO_B65_L3_AD15_V8_N <= s_sysDebug(13); --IOC7 
    
end structure;
