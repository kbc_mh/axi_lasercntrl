/******************************************************************************
*
* Copyright (C) 2019 Koenig & Bauer Coding GmbH
*
******************************************************************************/
/*****************************************************************************/
/**
* @file kbcLaserCntrl.c
* @{
*
* The implementation of the KBCLaserCntrl low level driver.
*
* @note
*
* None
*
* <pre>
* MODIFICATION HISTORY:
*
* Ver   Who  Date     Changes
* ----- ---- -------- -----------------------------------------------
* 1.00a mh2  13/05/19 First release
* </pre>
*
******************************************************************************/

/***************************** Include Files *******************************/
#include "kbcLaserCntrl.h"
/************************** Constant Definitions ****************************/

/**************************** Type Definitions ******************************/

/***************** Macros (Inline Functions) Definitions ********************/

/************************** Variable Definitions *****************************/
extern KBCLaserCntrl_Config KBCLaserCntrl_ConfigTable[XPAR_KBCLASERCNTRL_NUM_INSTANCES];

/************************** Function Prototypes *****************************/



/******************************************************************************/
/**
* Lookup the device configuration based on the unique device ID. The table
* ConfigTable contains the configuration info for each device in the system.
*
* @param	DeviceId is the device identifier to lookup.
*
* @return
*		 - A pointer of data type KBCLaserCnrtl_Config which points to the
*		device configuration if DeviceID is found.
* 		- NULL if DeviceID is not found.
*
* @note		None.
*
******************************************************************************/
KBCLaserCntrl_Config *KBCLaserCntrl_LookupConfig(u16 DeviceId)
{
	KBCLaserCntrl_Config *pCfg = NULL;

	int Index;

	for (Index = 0; Index < XPAR_KBCLASERCNTRL_NUM_INSTANCES; Index++) {
		if (KBCLaserCntrl_ConfigTable[Index].DeviceId == DeviceId) {
			pCfg = &KBCLaserCntrl_ConfigTable[Index];
			break;
		}
	}

	return pCfg;
}

/****************************************************************************/
/**
* Initialize the KBCLaserCntrl instance provided by the caller based on the
* given configuration data.
*
* Nothing is done except to initialize the InstancePtr.
*
* @param	pInst is a pointer to an KBCLaserCntrl instance. The memory the
*		pointer references must be pre-allocated by the caller. Further
*		calls to manipulate the driver through the KBCLaserCntrl API must be
*		made with this pointer.
* @param	pConfig is a reference to a structure containing information
*		about a specific KBCLaserCntrl device. This function initializes an
*		pInst object for a specific device specified by the
*		contents of pConfig. This function can initialize multiple
*		instance objects with the use of multiple calls giving different
*		Config information on each call.
* @param 	EffectiveAddr is the device base address in the virtual memory
*		address space. The caller is responsible for keeping the address
*		mapping from EffectiveAddr to the device physical base address
*		unchanged once this function is invoked. Unexpected errors may
*		occur if the address mapping changes after this function is
*		called. If address translation is not used, use
*		Config->BaseAddress for this parameters, passing the physical
*		address instead.
*
* @return
* 		- XST_SUCCESS if the initialization is successfull.
*
* @note		None.
*
*****************************************************************************/
int KBCLaserCntrl_CfgInitialize(KBCLaserCntrl* pInst, KBCLaserCntrl_Config* pConfig,
			UINTPTR EffectiveAddr)
{
	/* Assert arguments */
	Xil_AssertNonvoid(pInst != NULL);

	/* Set some default values. */
	pInst->BaseAddress = EffectiveAddr;

	/*
	 * Indicate the instance is now ready to use, initialized without error
	 */
	pInst->IsReady = XIL_COMPONENT_IS_READY;
	return (XST_SUCCESS);
}


/****************************************************************************/
/**
* Initialize the KBCLaserCntrl instance provided by the caller based on the
* given DeviceID.
*
* Nothing is done except to initialize the Instance pointer.
*
* @param	pInst is a pointer to an KBCLaserCntrl instance. The memory the
*		pointer references must be pre-allocated by the caller. Further
*		calls to manipulate the instance/driver through the KBCLaserCntrl API
*		must be made with this pointer.
* @param	DeviceId is the unique id of the device controlled by this
* 		KBCLaserCntrl instance. Passing in a device id associates the generic
* 		KBCLaserCntrl instance to a specific device, as chosen by the caller or
*		application developer.
*
* @return
*		- XST_SUCCESS if the initialization was successful.
* 		- XST_DEVICE_NOT_FOUND  if the device configuration data was not
*		found for a device with the supplied device ID.
*
* @note		None.
*
*****************************************************************************/
int KBCLaserCntrl_Initialize(KBCLaserCntrl* pInst, u16 DeviceId)
{
	KBCLaserCntrl_Config *pConfig;

	/*
	 * Assert arguments
	 */
	Xil_AssertNonvoid(pInst != NULL);

	/*
	 * Lookup configuration data in the device configuration table.
	 * Use this configuration info down below when initializing this
	 * driver.
	 */
	pConfig = KBCLaserCntrl_LookupConfig(DeviceId);
	if (pConfig == (KBCLaserCntrl_Config *) NULL) {
		pInst->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return KBCLaserCntrl_CfgInitialize(pInst, pConfig,
			pConfig->BaseAddress);
}


/****************************************************************************/
/**
* Set the pulse generator input source of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Source is the data source to be used for the laser power setpoint
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetLaserSrc(KBCLaserCntrl *pInst, KBCLaserCntrl_LasSrc Source)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCLaserCntrl_mReadReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET);

	Reg = ((Reg & ~KBCLASERCNTRL_LASSRC_MASK) | ((Source & 0x1)<<KBCLASERCNTRL_LASSRC_SHIFT));

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the laser mode of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Mode is the laser mode to be used by the laser pulse generator
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetLaserMode(KBCLaserCntrl *pInst, KBCLaserCntrl_LasMode Mode)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCLaserCntrl_mReadReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET);

	Reg = ((Reg & ~KBCLASERCNTRL_LASMODE_MASK) | ((Mode & 0xF)<<KBCLASERCNTRL_LASMODE_SHIFT));

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the laser mode of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Enable sets the working state of the Laser pulse generator
* 			When enabled, the laser is in standby state, i.e. tickle pulses
* 			are generated in CO2-Mode !! Chance of laser leakage !!.
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetLaserEnable(KBCLaserCntrl *pInst, u32 Enable)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCLaserCntrl_mReadReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET);

	Reg = ((Reg & ~KBCLASERCNTRL_LASEN_MASK) | ((Enable & 0x1)<<KBCLASERCNTRL_LASEN_SHIFT));

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the laser mode of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Enable configures the standby pulse generator
* 			When enabled, the 1st standby pulse after a laserOn-Window is
*	 		generated exactly one standby pulse period after the after the
*	 		laser power state is set to low
*	 		When disabled, the standby pulse generator continuously run in
*	 		background i.e. the timing between standby pulses and laserOn-Windows
*	 		is random.
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetStdbyAlign(KBCLaserCntrl *pInst, u32 Enable)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCLaserCntrl_mReadReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET);

	Reg = ((Reg & ~KBCLASERCNTRL_SPALGN_MASK) | ((Enable & 0x1)<<KBCLASERCNTRL_SPALGN_SHIFT));

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the standby pulse settings of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Length is the duration of the standby pulses in �s
* @param	Period is the period of the standby pulses in �s
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetStdbyPulse(KBCLaserCntrl *pInst, float Length, float Period)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = (u32)(Length/1e6*(float)KBCLASERCNTRL_PULSEGEN_FREQ);
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_SPL_OFFSET, Reg);

	Reg = (u32)(Period/1e6*(float)KBCLASERCNTRL_PULSEGEN_FREQ);
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_SPP_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the On-pulse (PWM, Qswitch) settings of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Length is the duration of the On-pulses in �s
* @param	Period is the period of the On-pulses in �s
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetOnPulse(KBCLaserCntrl *pInst, float Length, float Period)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = (u32)(Length/1e6*(float)KBCLASERCNTRL_PULSEGEN_FREQ);
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_ONPL_OFFSET, Reg);

	Reg = (u32)(Period/1e6*(float)KBCLASERCNTRL_PULSEGEN_FREQ);
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_ONPP_OFFSET, Reg);
}


/****************************************************************************/
/**
* Set the On-pulse period of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	period is the pulse period
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetOnPulsePeriod(KBCLaserCntrl *pInst, float Period)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = (u32)(Period/1e6*(float)KBCLASERCNTRL_PULSEGEN_FREQ);
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_ONPP_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the On-pulse period of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Legnth is the pulse length
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetOnPulseLength(KBCLaserCntrl *pInst, float Length)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = (u32)(Length/1e6*(float)KBCLASERCNTRL_PULSEGEN_FREQ);
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_ONPL_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the On-pulse period of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Legnth is the pulse length
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetLaserState(KBCLaserCntrl *pInst, u32 State, float Delay)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = (u32)(Delay/1e6*(float)KBCLASERCNTRL_PULSEGEN_FREQ) | ((State & 0x1) * 0x80000000);
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CMD_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the analog (DAC) data input source of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	ch is the DAC channel
* @param	Source is the data source to be used for the DACs
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetAnalogSrc(KBCLaserCntrl *pInst, u32 Ch, KBCLaserCntrl_LasAuxSrc Source)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCLaserCntrl_mReadReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET);

	if(Ch == 0)	Reg = ((Reg & ~KBCLASERCNTRL_AN0SRC_MASK) | ((Source & 0x3)<<KBCLASERCNTRL_AN0SRC_SHIFT));
	if(Ch == 1)	Reg = ((Reg & ~KBCLASERCNTRL_AN1SRC_MASK) | ((Source & 0x3)<<KBCLASERCNTRL_AN1SRC_SHIFT));

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the DAC update config of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Mode is the data update mode of the DAC
* 			This setting is only used for data from within the PL
* 			Updates from sw can be configured freely
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetAnalogMode(KBCLaserCntrl *pInst, KBCLaserCntrl_AnOutMode Mode)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCLaserCntrl_mReadReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET);

	Reg = ((Reg & ~KBCLASERCNTRL_ANMODE_MASK) | ((Mode & 0x3)<<KBCLASERCNTRL_ANMODE_SHIFT));

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET, Reg);
}


/****************************************************************************/
/**
* Write data to the DAC outputs of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Ch is the DAC channel to be updated
* @param	Data is the data for the DAC
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_WriteAnalogData(KBCLaserCntrl *pInst, u32 Ch, u32 Data, u32 Update)
{
	u32 Reg;
	u32 Cmd;
	u32 Addr;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Cmd = (Update & 0x1) << 1; //Write reg and update all when "1"
	Addr = (Ch & 0x1);
	Reg = (Cmd << 19) | (Addr << 16) | (Data & 0xFFFF);

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_ADATA_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the digital data source of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Source is the data source to be used for the digital output
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_SetDigitalSrc(KBCLaserCntrl *pInst, KBCLaserCntrl_LasSrc Source)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCLaserCntrl_mReadReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET);
	Reg = ((Reg & ~KBCLASERCNTRL_DIGSRC_MASK) | ((Source & 0x3)<<KBCLASERCNTRL_DIGSRC_SHIFT));
	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Write data to the DAC outputs of the KBCLaserCntrl device
*
* @param	pInst is a pointer to an KBCLaserCntrl instance to be worked on.
* @param	Data is the data for the digital 8Bit-port
*
* @return	None.
*
*****************************************************************************/
void KBCLaserCntrl_WriteDigitalData(KBCLaserCntrl *pInst, u32 Data)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = (Data & 0xFF);

	KBCLaserCntrl_mWriteReg(pInst->BaseAddress, KBCLASERCNTRL_ADATA_OFFSET, Reg);
}
