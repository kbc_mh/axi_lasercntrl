

proc generate {drv_handle} {
	xdefine_include_file $drv_handle "xparameters.h" "KBCLaserCntrl" "NUM_INSTANCES" "DEVICE_ID"  "BASEADDR" "HIGHADDR"
	
	xdefine_config_file $drv_handle "kbcLaserCntrl_g.c" "KBCLaserCntrl" "DEVICE_ID" "BASEADDR"
}
