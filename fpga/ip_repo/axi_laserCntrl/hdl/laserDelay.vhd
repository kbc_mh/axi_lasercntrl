----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.05.2019 10:52:52
-- Design Name: 
-- Module Name: laserDelay - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity laserDelay is
  Generic (
    G_COUNTER_WIDTH : integer := 25
    );
  Port ( 
    clk_i : in STD_LOGIC;
    signal_i : in STD_LOGIC;
    delay_i : in STD_LOGIC_VECTOR (G_COUNTER_WIDTH-1 downto 0);
    signal_o : out STD_LOGIC
    );
end laserDelay;

architecture Behavioral of laserDelay is
  
  COMPONENT downCounter
  PORT (
    CLK : IN STD_LOGIC;
    LOAD : IN STD_LOGIC;
    L : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    THRESH0 : OUT STD_LOGIC;
    Q : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
  );
  END COMPONENT;

  type t_state is (idle,waiting);
  signal s_state_r : t_state := idle;

  signal s_dlyLval : std_logic_vector(47 downto 0); 
  signal s_dlyLoad_r : std_logic := '0';
  signal s_dlyTrshld : std_logic; 
  signal s_delay_r : std_logic_vector(G_COUNTER_WIDTH-1 downto 0);
  
  signal s_sigIn_r : std_logic := '0';  
  signal s_sigOut_r : std_logic := '0';
  signal s_signalRe : std_logic := '0';
  signal s_signalFe : std_logic := '0';
  signal s_edgeType_r : std_logic := '0';
  
  
begin

  s_dlyLval <= (s_dlyLval'high downto G_COUNTER_WIDTH => '0') & delay_i; 

  delayCnt_inst : downCounter
  PORT MAP (
    CLK => clk_i,
    LOAD => s_dlyLoad_r,
    L =>  s_dlyLval,
    THRESH0 => s_dlyTrshld,
    Q => open
  );  
  
  --Edge detection
  process(clk_i)
  begin
    if rising_edge(clk_i) then
      s_sigIn_r <= signal_i;
    end if;
  end process;
  
  s_signalRe <= signal_i and not s_sigIn_r;
  s_signalFe <= s_sigIn_r and not signal_i;



  --State machine
  process(clk_i)
  begin
    if rising_edge(clk_i) then
      s_dlyLoad_r <= '0';    
      case s_state_r is 
      when idle =>
        if(s_signalRe = '1' or s_signalFe = '1') then
          s_edgeType_r <= s_signalRe;          
          s_delay_r <= delay_i;
          s_dlyLoad_r <= '1';
          s_state_r <= waiting;            
        end if;
      when waiting =>
        if(unsigned(s_delay_r) = 0 or s_dlyTrshld = '1') then
          s_sigOut_r <= s_edgeType_r;
          s_state_r <= idle; 
        end if;       
      end case;
    end if;
  end process;

  signal_o <= s_sigOut_r;
  
end Behavioral;
