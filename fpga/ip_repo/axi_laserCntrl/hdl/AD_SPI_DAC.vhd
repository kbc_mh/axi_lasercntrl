----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.04.2019 11:56:15
-- Design Name: 
-- Module Name: AD_SPI_DAC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AD_SPI_DAC is
  Generic(
    G_CLK_DIV : integer := 4    
    );
  Port ( clk_i : in STD_LOGIC;
         data_i : in STD_LOGIC_VECTOR (23 downto 0);
         strb_i : in STD_LOGIC;
         sclk_o : out STD_LOGIC;
         sync_o : out STD_LOGIC;
         sdata_o : out STD_LOGIC;
         ready_o : out STD_LOGIC
         );
end AD_SPI_DAC;

architecture Behavioral of AD_SPI_DAC is

type t_clkState is (idle,clocking);
signal s_clkState_r : t_clkState := idle; 

type t_shiftState is (idle,sync,shift);
signal s_state_r : t_shiftState := idle;

signal s_sync_r : std_logic := '1';

signal s_clkDivCnt_r : unsigned(5 downto 0) := (others => '0');
signal s_bitCnt_r : unsigned(4 downto 0) := (others => '0');

signal s_sclk_r : std_logic := '1';  --serial clock signal
signal s_sclk_dr : std_logic := '1'; --serial clock signal (delayed, aligned to sdata) 
signal s_sclk1st : std_logic; -- 1st edge pulse
signal s_sclk2nd : std_logic; -- 2nd edge pulse

signal s_data_r : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
signal s_shift_r : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');

signal s_ready : std_logic;

begin

process(clk_i)
begin
  if(rising_edge(clk_i)) then   
    if(strb_i = '1') then
      s_data_r <= data_i;
    end if;
  end if;
end process; 

sclkGen: -- generate serial clock
process(clk_i)
begin
  if(rising_edge(clk_i)) then     
    case s_clkState_r is 
    when idle =>
      if(strb_i = '1') then                     
        s_clkDivCnt_r <= (shift_right(to_unsigned(G_CLK_DIV,6),1));
        s_bitCnt_r <= to_unsigned(24,5);                   
        s_sclk_r <= '1';
        s_clkState_r <= clocking;
      end if;
    when clocking =>                
      if(s_clkDivCnt_r = 1) then                                                      
        s_clkDivCnt_r <= (shift_right(to_unsigned(G_CLK_DIV,6),1));
        s_sclk_r <= not(s_sclk_r);                         
                 
        if(s_sclk_r = '0') then              
          if(s_bitCnt_r > 0) then  
            s_bitCnt_r <= s_bitCnt_r - 1;    
          end if;                       
          if(s_bitCnt_r < 2) then              
              s_clkState_r <= idle;
          end if;                                                                                                                
        end if;
      else
        s_clkDivCnt_r <= s_clkDivCnt_r - 1;                                                                  
      end if;        
    end case;
    s_sclk_dr <= s_sclk_r;
  end if;
end process;

s_sclk1st <= s_sclk_r and not s_sclk_dr; -- 1st sclk edge pulse (rising)
s_sclk2nd <= s_sclk_dr and not s_sclk_r; -- 2nd sclk edge pulse (falling)

sync_shift: -- generate Sync signal and shift out data 
process(clk_i)
begin
  if(rising_edge(clk_i)) then
    case s_state_r is
    when idle =>
      if(strb_i = '1') then
        s_sync_r <= '0';    
        s_shift_r <= data_i;         
        s_state_r <= sync;  
      end if; 
    when sync =>
      if(s_sclk2nd = '1') then           
        s_state_r <= shift;  
      end if;
    when shift =>
      if(s_sclk1st = '1') then
        s_shift_r <= s_shift_r(22 downto 0) & '0';        
        if(s_bitCnt_r = 0) then                                          
          s_sync_r <= '1';
          s_state_r <= idle;     
        end if;          
      end if;       
    end case;
  end if;
end process;

s_ready <= '1' when s_state_r = idle else 
           '0';

sclk_o <= s_sclk_dr;
sync_o <= s_sync_r;    
sdata_o <= s_shift_r(23);

ready_o <= s_ready;

end Behavioral;
