----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.04.2019 16:19:17
-- Design Name: 
-- Module Name: DAC_AD56x3 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DAC_AD56x3 is
  Generic(
    G_CLK_DIV : integer := 4        
    );
  Port ( 
    clk_i : in STD_LOGIC;             
    strb_i : in STD_LOGIC;
    rawSelect_i : in STD_LOGIC;
    rawData_i : in STD_LOGIC_VECTOR (23 downto 0);    
    chData_i : in STD_LOGIC_VECTOR (15 downto 0);    
    chSelect_i : in STD_LOGIC;
    chUpdate_i : in STD_LOGIC;
    sclk_o : out STD_LOGIC;
    sync_o : out STD_LOGIC;
    sdata_o : out STD_LOGIC;
    ready_o : out STD_LOGIC
    );
end DAC_AD56x3;

architecture Behavioral of DAC_AD56x3 is
  
signal s_data : std_logic_vector(23 downto 0) := (others => '0');   
  
begin

s_data <= "000" & chUpdate_i & "000" & chSelect_i & chData_i when (rawSelect_i = '0') else
          rawData_i; 
  
spi_inst: entity work.AD_SPI_DAC
  Generic map(
    G_CLK_DIV => G_CLK_DIV
    )
  Port map( 
    clk_i => clk_i,
    data_i => s_data,
    strb_i => strb_i,
    sclk_o => sclk_o,
    sync_o => sync_o,
    sdata_o => sdata_o,
    ready_o => ready_o
    );
    
end Behavioral;
