----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.04.2019 15:12:19
-- Design Name: 
-- Module Name: laserAnalogOut - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.usr_pkg.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity laserAnalogOut is
  Generic(
    G_CLK_DIV : integer := 4        
    );
  Port ( 
    clk_i : in STD_LOGIC;
    axiData_i : in STD_LOGIC_VECTOR(23 downto 0);
    axiStrb_i : in STD_LOGIC;
    pwrData_i : in STD_LOGIC_VECTOR(15 downto 0);
    pwrStrb_i : in STD_LOGIC;
    arbCfg_i : in STD_LOGIC_VECTOR(1 downto 0);
    ch0Data_i : in STD_LOGIC_VECTOR(15 downto 0);
    ch0Strb_i : in std_logic;
    ch0Src_i : in STD_LOGIC_VECTOR(1 downto 0);    
    ch1Data_i : in STD_LOGIC_VECTOR(15 downto 0);
    ch1Strb_i : in std_logic;
    ch1Src_i : in STD_LOGIC_VECTOR(1 downto 0);    
    ch0Rdy_o : out STD_LOGIC;     
    ch1Rdy_o : out STD_LOGIC;
    sclk_o : out STD_LOGIC;
    sync_o : out STD_LOGIC;
    sdata_o : out STD_LOGIC;
    debug_o : out std_logic_vector(15 downto 0)       
    );
end laserAnalogOut;

architecture Behavioral of laserAnalogOut is

signal s_strb_r : std_logic := '0';

signal s_outData_r : std_logic_vector(23 downto 0) := (others => '0');
signal s_chData_r : slv24_array(1 downto 0) := ((others => '0'),(others => '0'));

signal s_chSelect_r : std_logic := '0';
signal s_chUpdate_r : std_logic := '0';

signal s_spiRdy : std_logic;
signal s_rdy_r : std_logic := '1';

signal s_ch0Strb_r : std_logic := '0';
signal s_ch1Strb_r : std_logic := '0'; 

signal s_chReqVec_r : std_logic_vector(1 downto 0) := (others => '0');

begin

Ch0_proc:
process(clk_i)
begin
  if(rising_edge(clk_i)) then
    if(s_chReqVec_r(0) = '0') then
      s_ch0Strb_r <= '0';  
      case ch0Src_i is
        when "01" => -- Laser Power
          if(pwrStrb_i = '1') then
            s_ch0Strb_r <= '1';          
            s_chData_r(0) <= "00000000" & pwrData_i;          
          end if;
        when "10" => -- AXI Data
          if(axiStrb_i = '1') then
            if(axiData_i(22) = '0' and axiData_i(19 downto 17) = "000") then -- verify command and address
              s_ch0Strb_r <= '1';            
              s_chData_r(0) <= axiData_i; 
            end if; 
          end if;
        when "11" => -- PL Data  
          if(ch0Strb_i = '1') then
            s_ch0Strb_r <= '1';          
            s_chData_r(0) <= "00000000" & ch0Data_i;   
          end if;
        when others => -- Disabled                
      end case;
    end if;   
  end if;
end process;

Ch1_proc:
process(clk_i)
begin
  if(rising_edge(clk_i)) then
    if(s_chReqVec_r(1) = '0') then
      s_ch1Strb_r <= '0';
      case ch1Src_i is
        when "01" => -- Laser Power
          if(pwrStrb_i = '1') then
            s_ch1Strb_r <= '1';
            s_chData_r(1) <= "00000001" & pwrData_i;         
          end if;
        when "10" => -- AXI Data
          if(axiStrb_i = '1') then
            if(axiData_i(22) = '0' and axiData_i(19 downto 17) = "001") then -- verify command and address
              s_ch1Strb_r <= '1';
              s_chData_r(1) <= axiData_i; 
            end if; 
          end if;
        when "11" => -- PL Data  
          if(ch1Strb_i = '1') then
            s_ch1Strb_r <= '1';
            s_chData_r(1) <= "00000001" & ch1Data_i;
          end if;
        when others => -- Disabled        
      end case;
    end if;   
  end if;
end process;

arbiter_proc:
process(clk_i)

variable v_arb : integer := 0;
variable v_SimulStepCnt : integer := 0;

begin
  if(rising_edge(clk_i)) then
    -- Set defaults
    s_chUpdate_r <= '0'; 
    s_strb_r <= '0'; 
    
    if(s_ch0Strb_r = '1') then
      s_chReqVec_r(0) <= '1';
    end if;
    
    if(s_ch1Strb_r = '1') then
      s_chReqVec_r(1) <= '1';
    end if;        
    
    if(s_spiRdy = '1' and s_strb_r = '0') then
      case arbCfg_i is
        -- When independent update is selected, constantly test for new data
        -- directly send single data frame and update DAC
        when "00" => 
          v_SimulStepCnt := 0;          
          if(s_chReqVec_r(v_arb) = '1') then
            s_chReqVec_r(v_arb) <= '0';
            s_outData_r <= s_chData_r(v_arb) OR x"100000";
            s_strb_r <= '1';
          end if;                 
        -- When simultaneous update is selected, wait until all channels hold 
        -- new data, than send all data in a row and update DAC with last frame  
        when "10" =>
          if(v_SimulStepCnt = 0) then          
            if(s_chReqVec_r = "11") then
              s_chReqVec_r(0) <= '0';
              s_outData_r <= s_chData_r(0);
              s_strb_r <= '1';
              v_SimulStepCnt := 1;
            end if;
          else
            s_chReqVec_r(1) <= '0';
            s_outData_r <= s_chData_r(1) OR x"100000";
            s_strb_r <= '1';
            v_SimulStepCnt := 0;       
          end if;
        when others =>        
      end case;      
      v_arb := (v_arb + 1) mod 2;                       
    end if;
  end if;
end process;

ch0Rdy_o <= not s_chReqVec_r(0);
ch1Rdy_o <= not s_chReqVec_r(1);

spi_inst: entity work.AD_SPI_DAC
  Generic map(
    G_CLK_DIV => G_CLK_DIV
    )
  Port map( 
    clk_i => clk_i,
    data_i => s_outData_r,
    strb_i => s_strb_r,
    sclk_o => sclk_o,
    sync_o => sync_o,
    sdata_o => sdata_o,
    ready_o => s_spiRdy
    );
    
  debug_o(1 downto 0) <= ch0Src_i;
  debug_o(2) <= pwrStrb_i;
  debug_o(3) <= axiStrb_i;
  debug_o(4) <= ch0Strb_i;
  debug_o(5) <= s_ch0Strb_r; 
  debug_o(6) <= s_strb_r;

end Behavioral;
