----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.04.2019 14:04:01
-- Design Name: 
-- Module Name: pwm28bitLatePulse - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pwmCore is
    Generic (
           G_COUNTER_WIDTH : integer := 25;
           G_LATE_PULSE : boolean := False
           );
    Port ( clk_i : in STD_LOGIC;
           srst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           period_i : in STD_LOGIC_VECTOR (G_COUNTER_WIDTH-1 downto 0);
           length_i : in STD_LOGIC_VECTOR (G_COUNTER_WIDTH-1 downto 0);
           signal_o : out STD_LOGIC);
end pwmCore;

architecture Behavioral of pwmCore is

  COMPONENT downCounter
  PORT (
    CLK : IN STD_LOGIC;
    LOAD : IN STD_LOGIC;
    L : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    THRESH0 : OUT STD_LOGIC;
    Q : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
  );
  END COMPONENT;

  type t_pulseState is (init,active,waiting);
  signal s_state_r : t_pulseState := init;
  signal s_state_dr : t_pulseState := init;

  signal s_perCntTrshld : std_logic;
  signal s_lenCntTrshld : std_logic;
  
  signal s_perCntLoad : std_logic := '0';
  signal s_lenCntLoad : std_logic := '0';
  
  signal s_period : unsigned(G_COUNTER_WIDTH-1 downto 0);
  signal s_period_r : unsigned(G_COUNTER_WIDTH-1 downto 0) := (others => '0');  
  signal s_periodLval : std_logic_vector(47 downto 0);
  
  signal s_length : unsigned(G_COUNTER_WIDTH-1 downto 0);  
  signal s_length_r : unsigned(G_COUNTER_WIDTH-1 downto 0) := (others => '0');
  signal s_lengthLval : std_logic_vector(47 downto 0);

  signal s_en_r : std_logic := '0';
  signal s_trigger : std_logic;
  
  signal s_pulse_r : std_logic := '0';
  signal s_pulsePol : std_logic;
  
begin
  ASSERT (G_COUNTER_WIDTH <= 47) 
    REPORT "Counter width > 47, does not meet DSP48 based implementation"
      SEVERITY ERROR;
      
  s_period <= unsigned(period_i);   
     
  s_periodLval <= (s_periodLval'high downto G_COUNTER_WIDTH => '0') & std_logic_vector(s_period_r);
  
  latePulseGen: if(G_LATE_PULSE = True) generate
    s_pulsePol <=  '0';
    s_length <= (unsigned(period_i) - unsigned(length_i))
                      when (unsigned(length_i) < unsigned(period_i)) else
                (others => '0');      
  end generate latePulseGen; 
  
  earlyPulseGen: if(G_LATE_PULSE = False) generate   
    s_pulsePol <=  '1';
    s_length <= unsigned(length_i) when (unsigned(length_i) <= unsigned(period_i)) else
                unsigned(period_i);        
  end generate earlyPulseGen; 
    
  s_lengthLval <= (s_lengthLval'high downto G_COUNTER_WIDTH => '0') & std_logic_vector(s_length); 
                 
  PeriodCount_inst : downCounter
  PORT MAP (
    CLK => clk_i,
    LOAD => s_perCntLoad,
    L => std_logic_vector(s_periodLval),
    THRESH0 => s_perCntTrshld,
    Q => open
  );  
  
  s_perCntLoad <= s_perCntTrshld or not s_en_r;
  
  LengthCount_inst : downCounter
  PORT MAP (
    CLK => clk_i,
    LOAD => s_lenCntLoad,
    L => std_logic_vector(s_lengthLval),
    THRESH0 => s_lenCntTrshld,
    Q => open
  );
  
  s_lenCntLoad <= s_perCntTrshld or not s_en_r;
  
  s_trigger <= en_i and not s_en_r;
  
  process(clk_i)
  begin  
    if(rising_edge(clk_i)) then
      s_state_dr <= s_state_r;
      if((s_perCntTrshld = '1') or (en_i = '0')) then
        s_length_r <= s_length;
        s_period_r <= s_period;        
      end if; 
    end if;
  end process;
  
  
  process(clk_i)
  begin  
    if(rising_edge(clk_i)) then
      s_en_r <= En_i;      
         
      if(en_i = '1') then  -- run FSM only when PWM is enabled               
        case s_State_r is                          
          when init =>  -- Initialize PWM      
            if(s_length_r = 0) then -- special treatment for 0% duty cycle
              s_state_r <= waiting;
            else -- normal operation        
              s_pulse_r <= s_pulsePol; -- set output
              s_state_r <= active;
            end if;            
          when active => -- Pulse is on                                                                          
            if(s_period_r = s_length_r) then -- special treatment for 100% duty cycle
              s_pulse_r <= s_pulsePol;                                                           
            elsif(s_lenCntTrshld = '1') then -- normal operation; pulse end reached                                                       
               s_pulse_r <= not s_pulsePol; -- reset output       
              s_state_r <= waiting; 
            else
              s_pulse_r <= s_pulsePol; -- keep output set             
            end if;
          
          when waiting => -- wait for end cycle
            s_pulse_r <= not s_pulsePol; -- reset output                          
            if(s_perCntTrshld = '1') then -- at the end of each period 
              if(s_length /= 0) then -- if duty cycle is 0% duty not leave waiting state
                -- normal operation
                s_pulse_r <= s_pulsePol; -- set output
                s_state_r <= active; -- switch to active state                 
              end if;           
            end if;
        end case;
        
      else -- when pwm is disable prepare fsm initialization  
        s_pulse_r <= '0'; 
        s_state_r <= init;     
      end if;            
    end if;
  end process;     

  signal_o <= s_pulse_r;

end Behavioral;
