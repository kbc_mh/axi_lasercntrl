library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.usr_pkg.all;

entity axi_laserCntrl_v1_0 is
	generic (
		-- Users to add parameters here
    G_DAC_CLK_DIV : integer := 4;
		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
    -- qsPeriod_i : in std_logic_vector(24 downto 0);
    -- qsLength_i : in std_logic_vector(24 downto 0);
    -- stdbyPeriod_i : in std_logic_vector(24 downto 0);
    -- stdbyLength_i : in std_logic_vector(24 downto 0);
    -- laserGate_i : in std_logic;
    forceOff_i : in std_logic;
    --pwrData_i : in std_logic_vector(15 downto 0);
    --pwrStrb_i : in std_logic;
    an0Data_i : in std_logic_vector(15 downto 0);
    an0Strb_i : in std_logic;
    an1Data_i : in std_logic_vector(15 downto 0);
    an1Strb_i : in std_logic;
    digData_i : in std_logic_vector(7 downto 0);
    digStrb_i : in std_logic;
    digData_o : out std_logic_vector(7 downto 0);
    dacSclk_o : out std_logic;
    dacSdata_o : out std_logic;
    dacSync_o : out std_logic;
    laserOn_o : out std_logic;
    laser1_o : out std_logic;
    laser2_o : out std_logic; 
    debug_o : out std_logic_vector(15 downto 0);   
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end axi_laserCntrl_v1_0;

architecture arch_imp of axi_laserCntrl_v1_0 is

	-- component declaration
	component axi_laserCntrl_v1_0_S00_AXI is
		generic (
		G_N_REGS : integer := 16;
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
		slvReg_i : in std_logic_vector(31 downto 0);
    slvReg_o : out slv32_array(G_N_REGS-2 downto 0);
    slvRegUpdated_o : out std_logic_vector(G_N_REGS-2 downto 0);
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component axi_laserCntrl_v1_0_S00_AXI;

  constant C_N_REGS : integer := 16;

  signal s_config : slv32_array(C_N_REGS-2 downto 0);
  signal s_configUpdated : std_logic_vector(C_N_REGS-2 downto 0);
  signal s_status_r : std_logic_vector(31 downto 0) := (others => '0');
  
  signal s_cntrl : std_logic_vector(31 downto 0);
  signal s_pwrScale : std_logic_vector(31 downto 0);
  
  -- Config Reg Signals
  signal s_crMode : std_logic_vector(2 downto 0);
  signal s_crLasEn : std_logic;
  signal s_crStdbyAlign : std_logic;
  signal s_crLasSrc : std_logic;
  signal s_crDacArbCfg : std_logic_vector(1 downto 0);
  signal s_crDac0Src : std_logic_vector(1 downto 0);
  signal s_crDac1Src : std_logic_vector(1 downto 0);
  signal s_crDigSrc : std_logic_vector(1 downto 0);
  
  -- Power Cmd Reg Signals
  signal s_pcPwrState : std_logic;
  signal s_pcDelay : std_logic_vector(24 downto 0);
  
  -- AXI data reg signals
  signal s_axiAnData : std_logic_vector(31 downto 0); 
  signal s_axiAnStrb : std_logic; 
  signal s_axiDigData : std_logic_vector(31 downto 0);
  signal s_axiDigStrb : std_logic; 
  signal s_axiStdbyLength : std_logic_vector(31 downto 0);
  signal s_axiStdbyPeriod : std_logic_vector(31 downto 0);
  signal s_axiQsLength : std_logic_vector(31 downto 0);
  signal s_axiQsPeriod : std_logic_vector(31 downto 0);
  signal s_axiQsDelay : std_logic_vector(31 downto 0);
  signal s_axiFkpLength : std_logic_vector(31 downto 0);


  signal s_stdbyEn_r : std_logic := '0';
  signal s_lasOn_r : std_logic := '0';
  signal s_stdbyPeriod_r : std_logic_vector(24 downto 0) := (others => '0');
  signal s_stdbyLength_r : std_logic_vector(24 downto 0) := (others => '0');
  signal s_onPeriod_r : std_logic_vector(24 downto 0) := (others => '0');
  signal s_onLength_r : std_logic_vector(24 downto 0) := (others => '0');
 
  signal s_digData_r : std_logic_vector(7 downto 0) := (others => '0');
 
  signal s_dac0Rdy : std_logic;   
  signal s_dac1Rdy : std_logic;
  
  signal s_laserOn : std_logic;
  signal s_laser1 : std_logic;
  signal s_laser2 : std_logic;
  
  signal s_anDebug :  std_logic_vector(15 downto 0);
  
  
  
begin 
    
-- Instantiation of Axi Bus Interface S00_AXI
axi_laserCntrl_v1_0_S00_AXI_inst : axi_laserCntrl_v1_0_S00_AXI
	generic map (
	 G_N_REGS => C_N_REGS,
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
	  slvReg_i => s_status_r,
	  slvReg_o => s_config,
	  slvRegUpdated_o => s_configUpdated,
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

  -- Add user logic here
  
  
  -- AXI register mapping
  s_crMode <= s_config(0)(2 downto 0);
  s_crLasEn <= s_config(0)(4);
  s_crStdbyAlign <= s_config(0)(5);
  s_crLasSrc <= s_config(0)(6);
  s_crDacArbCfg <= s_config(0)(17 downto 16);
  s_crDac0Src <= s_config(0)(19 downto 18);
  s_crDac1Src <= s_config(0)(21 downto 20);
  s_crDigSrc <= s_config(0)(23 downto 22);
  
  s_pcPwrState <= s_config(1)(31);
  s_pcDelay <= s_config(1)(24 downto 0);
  
  s_axiStdbyLength <= s_config(4);
  s_axiStdbyPeriod <= s_config(5);
  
  s_axiQsLength <= s_config(6);
  s_axiQsPeriod <= s_config(7);
  
  s_axiAnData <= s_config(12);
  s_axiAnStrb <= s_configUpdated(12);
  
  s_axiDigData <= s_config(13);
  s_axiDigStrb <= s_configUpdated(13);
  
lasScrProc:  
  process(s00_axi_aclk)
  begin
    if(rising_edge(s00_axi_aclk)) then
      case s_crMode is
        when "001" => -- CO2
          s_stdbyEn_r <= s_crLasEn;
          if(s_crLasSrc = '1') then
            s_lasOn_r <= s_crLasEn and s_pcPwrState;
            s_stdbyLength_r <= s_axiStdbyLength(24 downto 0);
            s_stdbyPeriod_r <= s_axiStdbyPeriod(24 downto 0);
            s_onLength_r <= s_axiQsLength(24 downto 0);
            s_onPeriod_r <= s_axiQsPeriod(24 downto 0);
          else
            s_lasOn_r <= '0';
            s_stdbyLength_r <= (others => '0');
            s_stdbyPeriod_r <= (others => '0');
            s_onLength_r <= (others => '0');
            s_onPeriod_r <= (others => '0');
          end if;
        when others => -- disabled
          s_stdbyEn_r <= '0';
          s_lasOn_r <= '0';
          s_stdbyLength_r <= (others => '0');
          s_stdbyPeriod_r <= (others => '0');
          s_onLength_r <= (others => '0');
          s_onPeriod_r <= (others => '0');
      end case;
    end if;
  end process;  
   
digOutProc:
  process(s00_axi_aclk)
  begin
    if(rising_edge(s00_axi_aclk)) then
      case s_crDigSrc is
        when "01" => -- Laser Power
          if(TRUE) then --pwrStrb_i = '1') then
            s_digData_r <= (others => '0');--pwrData_i,
          end if;        
        when "10" => -- AXI Data
          if(s_axiDigStrb = '1') then
            s_digData_r <= s_axiDigData(7 downto 0);
          end if;
        when "11" => -- PL Data
          if(digStrb_i = '1') then
            s_digData_r <= digData_i;
          end if;
        when others => -- Disabled
          s_digData_r <= (others => '0');
      end case;  
    end if;
  end process;        
  
  digData_o <= s_digData_r;

statusRegProc:
  process(s00_axi_aclk)
  begin
    if(rising_edge(s00_axi_aclk)) then
      s_status_r <= (s_status_r'high downto 3 => '0') & forceOff_i & s_laserOn & s_lasOn_r;
    end if;
  end process;
    
pulseGen_inst : entity work.laserPulseGen
  Port map ( 
    clk_i => s00_axi_aclk,
    srst_i => '0',
    en_i => s_lasOn_r,       
    stdbyEn_i => s_stdbyEn_r,          
    forceOff_i => forceOff_i,
    onPeriod_i => s_onPeriod_r,           
    onLength_i => s_onLength_r,
    stdbyPeriod_i => s_stdbyPeriod_r,
    stdbyLength_i => s_stdbyLength_r,
    laserOn_o => s_laserOn,
    laser1_o => s_laser1,
    laser2_o => s_laser2
    );

  laserOn_o <= s_laserOn;
  laser1_o <= s_laser1;
  laser2_o <= s_laser2;

analogOut_inst : entity work.laserAnalogOut
  Generic map(
    G_CLK_DIV => G_DAC_CLK_DIV
    )
  Port map( 
    clk_i => s00_axi_aclk,
    axiData_i => s_axiAnData(23 downto 0),
    axiStrb_i => s_axiAnStrb,
    pwrData_i => (others => '0'),--pwrData_i,
    pwrStrb_i => '0',--pwrStrb_i,
    arbCfg_i => s_crDacArbCfg,
    ch0Data_i => an0Data_i,
    ch0Strb_i => an0Strb_i,
    ch0Src_i => s_crDac0Src,    
    ch1Data_i => an1Data_i,
    ch1Strb_i => an1Strb_i,
    ch1Src_i => s_crDac1Src,    
    ch0Rdy_o => s_dac0Rdy,
    ch1Rdy_o => s_dac1Rdy,
    sclk_o => dacSclk_o,
    sync_o => dacSync_o,
    sdata_o => dacSdata_o,
    debug_o => s_anDebug    
    );

  debug_o <= s_anDebug;
	-- User logic ends



end arch_imp;