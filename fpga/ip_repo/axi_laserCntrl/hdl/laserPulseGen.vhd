----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.04.2019 11:21:44
-- Design Name: 
-- Module Name: laserPulseGen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity laserPulseGen is
  Port(  
    clk_i : in STD_LOGIC;
    srst_i : in STD_LOGIC;
    en_i : in std_logic;            
    stdbyEn_i : in std_logic;          
    forceOff_i : in std_logic;
    onPeriod_i : in std_logic_vector(24 downto 0);           
    onLength_i : in std_logic_vector(24 downto 0);
    stdbyPeriod_i : in std_logic_vector(24 downto 0);
    stdbyLength_i : in std_logic_vector(24 downto 0);
    laserOn_o : out STD_LOGIC;
    laser1_o : out STD_LOGIC;
    laser2_o : out STD_LOGIC
    );
end laserPulseGen;

architecture Behavioral of laserPulseGen is
  
  signal s_stdbyPeriod : std_logic_vector(24 downto 0); 
  signal s_stdbyLength : std_logic_vector(24 downto 0); 
    
  signal s_onPeriod : std_logic_vector(24 downto 0); 
  signal s_onLength : std_logic_vector(24 downto 0);
  
  signal s_stdbyPulse : std_logic;
  signal s_onPulse : std_logic;
  signal s_outPulse_r : std_logic;
  
  signal s_en : std_logic;
  signal s_en_r : std_logic := '0';
  
  signal s_stdbyEn : std_logic; 
  
begin

  s_stdbyPeriod <= stdbyPeriod_i; 
  s_stdbyLength <= stdbyLength_i;
  
  s_stdbyEn <= stdbyEn_i and not forceOff_i;
  
  stdbyPulseGen_inst: entity work.pwmCore
  Generic map(
    G_COUNTER_WIDTH => 25,
    G_LATE_PULSE => TRUE
    )
  Port map( 
    clk_i => clk_i,
    srst_i => srst_i,
    en_i => s_stdbyEn,
    period_i => s_stdbyPeriod,
    length_i => s_stdbyLength,
    signal_o => s_stdbyPulse 
    );
  
  s_onPeriod <= onPeriod_i;
  s_onLength <= onLength_i; 
  
  s_en <= en_i and not forceOff_i;
  
  OnPulseGen_inst: entity work.pwmCore
  Generic map(
    G_COUNTER_WIDTH => 25,
    G_LATE_PULSE => FALSE
    )
  Port map( 
    clk_i => clk_i,
    srst_i => srst_i,
    en_i => s_en,
    period_i => s_onPeriod,
    length_i => s_onLength,
    signal_o => s_onPulse 
    );   

  process(clk_i)
  begin
    if(rising_edge(clk_i)) then
      s_en_r <= en_i;
      if(forceOff_i = '1') then
        s_outPulse_r <= '0';  
      elsif(s_en_r = '1') then
        s_outPulse_r <= s_onPulse;
      else
        s_outPulse_r <= s_stdbyPulse;   
      end if;
    end if;  
  end process; 

  laserOn_o <= s_en_r;
  laser1_o <= s_outPulse_r;

end Behavioral;
