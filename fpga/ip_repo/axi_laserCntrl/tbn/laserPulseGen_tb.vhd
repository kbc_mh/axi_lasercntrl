----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.09.2018 15:36:16
-- Design Name: 
-- Module Name: pulseDelay_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity laserPulseGen_tb is
--  Port ( );
end laserPulseGen_tb;

architecture Behavioral of laserPulseGen_tb is
    component laserPulseGen is
    Port ( clk_i : in STD_LOGIC;
           srst_i : in STD_LOGIC;
           en_i : in std_logic;            
           stdbyEn_i : in std_logic;          
           forceOff_i : in std_logic;
           onPeriod_i : in std_logic_vector(24 downto 0);           
           onLength_i : in std_logic_vector(24 downto 0);
           stdbyPeriod_i : in std_logic_vector(24 downto 0);
           stdbyLength_i : in std_logic_vector(24 downto 0);
           laserOn_o : out STD_LOGIC;
           laser1_o : out STD_LOGIC;
           laser2_o : out STD_LOGIC
           );
    end component laserPulseGen;
    
    constant C_SREG_WIDTH : natural := 24;
    
    signal clk_i : std_logic := '0';
    signal srst_i : std_logic := '0';
    
    signal en_i : std_logic := '0';
    signal stdbyEn_i : std_logic := '0';
    signal forceOff_i : std_logic := '0';
    
    signal onPeriod_i : std_logic_vector(24 downto 0) := std_logic_vector(to_unsigned(20,25));
    signal onLength_i : std_logic_vector(24 downto 0) := std_logic_vector(to_unsigned(10,25));
    
    signal stdbyPeriod_i : std_logic_vector(24 downto 0) := std_logic_vector(to_unsigned(50,25));
    signal stdbyLength_i : std_logic_vector(24 downto 0) := std_logic_vector(to_unsigned(5,25));
        
    signal laserOn_o : STD_LOGIC;
    signal laser1_o : STD_LOGIC;
    signal laser2_o : STD_LOGIC;    
        
    constant clk_period : time := 10ns;        

begin

    uut:  laserPulseGen 
    port map( 
        clk_i => clk_i,
        srst_i => srst_i,
        en_i => en_i,
        stdbyEn_i => stdbyEn_i,
        forceOff_i => forceOff_i,
        onPeriod_i => onPeriod_i,
        onLength_i => onLength_i,        
        stdbyPeriod_i => stdbyPeriod_i,
        stdbyLength_i => stdbyLength_i,        
        laserOn_o => laserOn_o,
        laser1_o => laser1_o,
        laser2_o => laser2_o              
        );
        
    --clk process
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;    
         

    -- stimulus
    process
    begin
        wait for clk_period*5;
        srst_i <= '1';
        wait for clk_period*10;
        srst_i <= '0';    
        
        wait for 123.5ns;
        
        stdbyEn_i <= '1';
        wait for clk_period*1024;
        
        stdbyEn_i <= '0';
        en_i <= '1';
        onLength_i <= std_logic_vector(to_unsigned(18,25));
        wait for clk_period*100;
        
        onLength_i <= std_logic_vector(to_unsigned(19,25));
        wait for clk_period*100;
        
        onLength_i <= std_logic_vector(to_unsigned(20,25));
        wait for clk_period*100;
        
        onLength_i <= std_logic_vector(to_unsigned(1,25));
        wait for clk_period*100;
        
        onLength_i <= std_logic_vector(to_unsigned(0,25));
        wait for clk_period*100;
        
        onLength_i <= std_logic_vector(to_unsigned(15,25));
        wait for clk_period*83;
        
        stdbyEn_i <= '1';
        en_i <= '0';
        wait for clk_period*968;
        
        en_i <= '1';
        wait for clk_period*683;
        
        
        wait;
    end process;
    
end Behavioral;
