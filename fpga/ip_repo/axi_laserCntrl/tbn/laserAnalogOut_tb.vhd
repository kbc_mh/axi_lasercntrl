----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.09.2018 15:36:16
-- Design Name: 
-- Module Name: pulseDelay_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity laserAnalogOut_tb is
--  Port ( );
end laserAnalogOut_tb;

architecture Behavioral of laserAnalogOut_tb is
  
  component laserAnalogOut is
  Generic(
    G_CLK_DIV : integer := 4        
    );
  Port ( 
    clk_i : in STD_LOGIC;
    axiData_i : in STD_LOGIC_VECTOR(23 downto 0);
    axiStrb_i : in STD_LOGIC;
    pwrData_i : in STD_LOGIC_VECTOR(15 downto 0);
    pwrStrb_i : in STD_LOGIC;
    arbCfg_i : in STD_LOGIC_VECTOR(1 downto 0);
    ch0Data_i : in STD_LOGIC_VECTOR(15 downto 0);
    ch0Strb_i : in std_logic;
    ch0Src_i : in STD_LOGIC_VECTOR(1 downto 0);    
    ch1Data_i : in STD_LOGIC_VECTOR(15 downto 0);
    ch1Strb_i : in std_logic;
    ch1Src_i : in STD_LOGIC_VECTOR(1 downto 0);    
    ch0Ready_o : out STD_LOGIC;     
    ch1Ready_o : out STD_LOGIC;
    sclk_o : out STD_LOGIC;
    sync_o : out STD_LOGIC;
    sdata_o : out STD_LOGIC         
    );
  end component laserAnalogOut;
    
    constant G_CLK_DIV : natural := 2;
    
    signal clk_i : std_logic := '0';
    signal axiStrb_i : std_logic := '0';

    signal pwrStrb_i : std_logic := '0';
    signal ch0Strb_i : std_logic := '0';
    signal ch1Strb_i : std_logic := '0';
    
    signal axiData_i : std_logic_vector(23 downto 0) := std_logic_vector(to_unsigned(20,24));
    signal pwrData_i : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(10,16));
    
    signal ch0Data_i : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(16#AAAA#,16));
    signal ch1Data_i : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(16#5555#,16));
    
    signal arbCfg_i : std_logic_vector(1 downto 0) := std_logic_vector(to_unsigned(0,2));
    signal ch0Src_i : std_logic_vector(1 downto 0) := std_logic_vector(to_unsigned(3,2));
    signal ch1Src_i : std_logic_vector(1 downto 0) := std_logic_vector(to_unsigned(3,2));    
        
    signal sclk_o : STD_LOGIC;
    signal sync_o : STD_LOGIC;
    signal sdata_o : STD_LOGIC; 
    
    signal ch0Ready_o : STD_LOGIC;    
    signal ch1Ready_o : STD_LOGIC;
        
    constant clk_period : time := 10ns;        

begin

    uut: laserAnalogOut 
    Generic map(
      G_CLK_DIV => G_CLK_DIV
      )
    Port map( 
      clk_i => clk_i,
      axiData_i => axiData_i,
      axiStrb_i => axiStrb_i,
      pwrData_i => pwrData_i,
      pwrStrb_i => pwrStrb_i,
      arbCfg_i => arbCfg_i,
      ch0Data_i => ch0Data_i,
      ch0Strb_i => ch0Strb_i,
      ch0Src_i => ch0Src_i,    
      ch1Data_i => ch1Data_i,
      ch1Strb_i => ch1Strb_i,
      ch1Src_i => ch1Src_i,    
      ch0Ready_o => ch0Ready_o,
      ch1Ready_o => ch1Ready_o,
      sclk_o => sclk_o,
      sync_o => sync_o,
      sdata_o => sdata_o           
    );
        
    --clk process
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;    
         

    -- stimulus
    process
    begin        
        wait for 123.5ns;
        
        ch0Strb_i <= '1';
        wait for clk_period;
        ch0Strb_i <= '0';
        
        wait for clk_period*5;
        
        ch1Strb_i <= '1';
        wait for clk_period;
        ch1Strb_i <= '0';
        
        wait for clk_period*120;
        
        ch0Strb_i <= '1';
        ch1Strb_i <= '1';
        wait for clk_period;
        ch0Strb_i <= '0';
        ch1Strb_i <= '0';
        
        wait for clk_period*120;
        
        ch0Strb_i <= '1';
        wait for clk_period;
        ch0Data_i <= std_logic_vector(to_unsigned(16#AAAB#,16));
        ch0Strb_i <= '0';
        wait for clk_period;
        ch0Strb_i <= '1';
        ch0Data_i <= std_logic_vector(to_unsigned(16#BBBB#,16));
        wait for clk_period;
        ch0Data_i <= std_logic_vector(to_unsigned(16#BBBC#,16));
        ch0Strb_i <= '0';
        wait for clk_period;
         ch0Strb_i <= '1';
         ch0Data_i <= std_logic_vector(to_unsigned(16#CCCC#,16));
        wait for clk_period;
        ch0Strb_i <= '0';
        wait for clk_period;
        
        wait for clk_period*54;
        
        
        ch0Strb_i <= '1';
        ch0Data_i <= std_logic_vector(to_unsigned(16#DDDD#,16));
        wait for clk_period;
        ch0Strb_i <= '0';
        
        wait for clk_period*54;
        
        arbCfg_i <= "10";
        
        ch0Strb_i <= '1';
        ch0Data_i <= std_logic_vector(to_unsigned(16#EEEE#,16));
        wait for clk_period;
        ch0Strb_i <= '0';
        
        wait for clk_period*13;
        
        ch1Strb_i <= '1';
        wait for clk_period;
        ch1Strb_i <= '0';
        
        wait;
    end process;
    
end Behavioral;
