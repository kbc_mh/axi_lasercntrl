----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.09.2018 15:36:16
-- Design Name: 
-- Module Name: pulseDelay_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ad_spi_dac_tb is
--  Port ( );
end ad_spi_dac_tb;

architecture Behavioral of ad_spi_dac_tb is    
    
  component AD_SPI_DAC is
  Generic(
    G_CLK_DIV : integer := 4    
    );
  Port ( clk_i : in STD_LOGIC;
         data_i : in STD_LOGIC_VECTOR (23 downto 0);
         strb_i : in STD_LOGIC;
         sclk_o : out STD_LOGIC;
         sync_o : out STD_LOGIC;
         sdata_o : out STD_LOGIC;
         ready_o : out STD_LOGIC
         );
  end component AD_SPI_DAC;
    
  constant C_CLK_DIV : natural := 2;
  
  signal clk_i : std_logic := '0';
  signal strb_i : std_logic := '0';
  
  signal data_i : std_logic_vector (23 downto 0) := std_logic_vector(to_unsigned(10,24));  

  signal sclk_o : STD_LOGIC;
  signal sync_o : STD_LOGIC;
  signal sdata_o : STD_LOGIC;       
  
  signal ready_o : STD_LOGIC;       
      
  constant clk_period : time := 10ns;        

begin

    uut:  AD_SPI_DAC 
    generic map(
      G_CLK_DIV => C_CLK_DIV
    )
    port map( 
        clk_i => clk_i,        
        strb_i => strb_i,       
        data_i => data_i,       
        sclk_o => sclk_o,
        sync_o => sync_o,
        sdata_o => sdata_o,
        ready_o => ready_o  
        );
        
    --clk process
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;    
         

    -- stimulus
    process
    begin
        wait for clk_period*5;
        
        wait for 123.5ns;
        
        data_i <= x"AAAAAA";

        wait for clk_period;   
                             
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        
        wait for clk_period*99;
        data_i <= x"555555";

        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*99;
        
       
        wait;
    end process;
    
end Behavioral;
