----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.09.2018 15:36:16
-- Design Name: 
-- Module Name: pulseDelay_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pwmCore_tb is
--  Port ( );
end pwmCore_tb;

architecture Behavioral of pwmCore_tb is        
    
    constant C_SREG_WIDTH : natural := 24;
    
    signal clk_i : std_logic := '0';
    signal srst_i : std_logic := '0';
        
    signal en_i : std_logic := '0';
    signal period_i : std_logic_vector(27 downto 0) := std_logic_vector(to_unsigned(20,28));
    signal length_i : std_logic_vector(27 downto 0) := std_logic_vector(to_unsigned(10,28));
   
    signal signal_o : STD_LOGIC;
  
        
    constant clk_period : time := 10ns;        

begin

    uut : entity work.pwmCore(Behavioral) 
    generic map(
        G_COUNTER_WIDTH => 28,
        G_LATE_PULSE => True
      )
    port map( 
        clk_i => clk_i,
        srst_i => srst_i,
        en_i => en_i,
        period_i => period_i,
        length_i => length_i,        
        signal_o => signal_o     
        );
        
    --clk process
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;    
         

    -- stimulus
    process
    begin
        wait for clk_period*5;
        srst_i <= '1';
        wait for clk_period*10;
        srst_i <= '0';    
        
        wait for 123.5ns;
        
        en_i <= '1';
        wait for clk_period*100;
        
        length_i <= std_logic_vector(to_unsigned(18,28));
        wait for clk_period*100;
        
        length_i <= std_logic_vector(to_unsigned(19,28));
        wait for clk_period*100;
        
        length_i <= std_logic_vector(to_unsigned(20,28));
        wait for clk_period*100;
        
        length_i <= std_logic_vector(to_unsigned(1,28));
        wait for clk_period*100;
        
        length_i <= std_logic_vector(to_unsigned(0,28));
        wait for clk_period*100;
        
        length_i <= std_logic_vector(to_unsigned(15,28));
        wait for clk_period*93;
        
        en_i <= '0';
        wait for clk_period*100;
        
        en_i <= '1';
        
        wait;
    end process;
    
end Behavioral;
