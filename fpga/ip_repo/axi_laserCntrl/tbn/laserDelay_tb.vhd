----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.09.2018 15:36:16
-- Design Name: 
-- Module Name: pulseDelay_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity laserDelay_tb is
--  Port ( );
end laserDelay_tb;

architecture Behavioral of laserDelay_tb is    
    
  component laserDelay is
  Generic (
    G_COUNTER_WIDTH : integer := 25
    );
  Port ( 
    clk_i : in STD_LOGIC;
    signal_i : in STD_LOGIC;
    delay_i : in STD_LOGIC_VECTOR (G_COUNTER_WIDTH-1 downto 0);
    signal_o : out STD_LOGIC
    );
  end component laserDelay;
    
  constant G_COUNTER_WIDTH : natural := 25;
  
  signal clk_i : std_logic := '0';
  signal signal_i : std_logic := '0';
  
  signal delay_i : std_logic_vector (G_COUNTER_WIDTH-1 downto 0) := std_logic_vector(to_unsigned(0,G_COUNTER_WIDTH));  

  signal signal_o : STD_LOGIC;
  
  constant clk_period : time := 10ns;        

begin

    uut:  laserDelay 
    generic map(
      G_COUNTER_WIDTH => G_COUNTER_WIDTH
    )
    port map( 
        clk_i => clk_i,        
        signal_i => signal_i,       
        delay_i => delay_i,       
        signal_o => signal_o
        );
        
    --clk process
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;    
         

    -- stimulus
    process
    begin
      wait for clk_period*5;
      
      wait for 123.5ns;
             
      wait for clk_period;   
                           
      signal_i <= '1';
      wait for clk_period*10;
      signal_i <= '0';
      
      wait for clk_period*10;
      signal_i <= '1';
      delay_i <= std_logic_vector(to_unsigned(0,G_COUNTER_WIDTH));
      wait for clk_period*10;
      signal_i <= '0';
      
      wait for clk_period*100;
      signal_i <= '1';
      delay_i <= std_logic_vector(to_unsigned(1,G_COUNTER_WIDTH));
      wait for clk_period*10;
      signal_i <= '0';
       
      wait for clk_period*100;
      signal_i <= '1';
      delay_i <= std_logic_vector(to_unsigned(2,G_COUNTER_WIDTH));
      wait for clk_period*10;
      signal_i <= '0';
      
      wait for clk_period*100;
      signal_i <= '1';
      delay_i <= std_logic_vector(to_unsigned(3,G_COUNTER_WIDTH));
      wait for clk_period*10;
      signal_i <= '0';
       
      wait for clk_period*100;
      signal_i <= '1'; 
      delay_i <= std_logic_vector(to_unsigned(4,G_COUNTER_WIDTH));
      wait for clk_period*10;      
      signal_i <= '0';
      
      wait for clk_period*100;
      signal_i <= '1'; 
      delay_i <= std_logic_vector(to_unsigned(0,G_COUNTER_WIDTH));
      wait for clk_period*10;      
      signal_i <= '0';
       
      wait;
    end process;
    
end Behavioral;
