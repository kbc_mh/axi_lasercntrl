# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "G_DAC_CLK_DIV" -parent ${Page_0} -widget comboBox


}

proc update_PARAM_VALUE.G_DAC_CLK_DIV { PARAM_VALUE.G_DAC_CLK_DIV } {
	# Procedure called to update G_DAC_CLK_DIV when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.G_DAC_CLK_DIV { PARAM_VALUE.G_DAC_CLK_DIV } {
	# Procedure called to validate G_DAC_CLK_DIV
	return true
}

proc update_PARAM_VALUE.BASEADDR { PARAM_VALUE.BASEADDR } {
	# Procedure called to update BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BASEADDR { PARAM_VALUE.BASEADDR } {
	# Procedure called to validate BASEADDR
	return true
}

proc update_PARAM_VALUE.HIGHADDR { PARAM_VALUE.HIGHADDR } {
	# Procedure called to update HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HIGHADDR { PARAM_VALUE.HIGHADDR } {
	# Procedure called to validate HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	# WARNING: There is no corresponding user parameter named "C_S00_AXI_DATA_WIDTH". Setting updated value from the model parameter.
set_property value 32 ${MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	# WARNING: There is no corresponding user parameter named "C_S00_AXI_ADDR_WIDTH". Setting updated value from the model parameter.
set_property value 6 ${MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.G_DAC_CLK_DIV { MODELPARAM_VALUE.G_DAC_CLK_DIV PARAM_VALUE.G_DAC_CLK_DIV } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.G_DAC_CLK_DIV}] ${MODELPARAM_VALUE.G_DAC_CLK_DIV}
}

