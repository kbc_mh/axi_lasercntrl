----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.04.2019 10:44:47
-- Design Name: 
-- Module Name: usr_pkg - 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package usr_pkg is

type slv2_array is array(integer range <>) of std_logic_vector(1 downto 0);
type slv3_array is array(integer range <>) of std_logic_vector(2 downto 0);
type slv4_array is array(integer range <>) of std_logic_vector(3 downto 0);
type slv5_array is array(integer range <>) of std_logic_vector(4 downto 0);
type slv6_array is array(integer range <>) of std_logic_vector(5 downto 0);
type slv7_array is array(integer range <>) of std_logic_vector(6 downto 0);
type slv8_array is array(integer range <>) of std_logic_vector(7 downto 0);
type slv9_array is array(integer range <>) of std_logic_vector(8 downto 0);
type slv10_array is array(integer range <>) of std_logic_vector(9 downto 0);
type slv11_array is array(integer range <>) of std_logic_vector(10 downto 0);
type slv12_array is array(integer range <>) of std_logic_vector(11 downto 0);
type slv13_array is array(integer range <>) of std_logic_vector(12 downto 0);
type slv14_array is array(integer range <>) of std_logic_vector(13 downto 0);
type slv15_array is array(integer range <>) of std_logic_vector(14 downto 0);
type slv16_array is array(integer range <>) of std_logic_vector(15 downto 0);
type slv17_array is array(integer range <>) of std_logic_vector(16 downto 0);
type slv18_array is array(integer range <>) of std_logic_vector(17 downto 0);
type slv19_array is array(integer range <>) of std_logic_vector(18 downto 0);
type slv20_array is array(integer range <>) of std_logic_vector(19 downto 0);
type slv21_array is array(integer range <>) of std_logic_vector(20 downto 0);
type slv22_array is array(integer range <>) of std_logic_vector(21 downto 0);
type slv23_array is array(integer range <>) of std_logic_vector(22 downto 0);
type slv24_array is array(integer range <>) of std_logic_vector(23 downto 0);
type slv25_array is array(integer range <>) of std_logic_vector(24 downto 0);
type slv26_array is array(integer range <>) of std_logic_vector(25 downto 0);
type slv27_array is array(integer range <>) of std_logic_vector(26 downto 0);
type slv28_array is array(integer range <>) of std_logic_vector(27 downto 0);
type slv29_array is array(integer range <>) of std_logic_vector(28 downto 0);
type slv30_array is array(integer range <>) of std_logic_vector(29 downto 0);
type slv31_array is array(integer range <>) of std_logic_vector(30 downto 0);
type slv32_array is array(integer range <>) of std_logic_vector(31 downto 0);

end package usr_pkg;


