@ECHO off
SETLOCAL EnableDelayedExpansion
cls
echo -----------------------------------
echo --                               -- 
echo -- Set VIVADO version            --
echo --                               --
echo -----------------------------------
set  VIVADO_VER=2018.3
echo. 
echo %VIVADO_VER%
echo.

IF EXIST w: (
   echo -------------------------------
   echo --                           --
   echo --  Drive W: already mapped  --
   echo --                           --
   echo -------------------------------
   
   CHOICE /C YNA /N /M "Remap? (Y)es (N)o (A)bort"

   if ERRORLEVEL 3 GOTO END
   if ERRORLEVEL 2 GOTO RUN
   if ERRORLEVEL 1 GOTO REMAP
) ELSE (
   subst W: %cd%
   GOTO RUN
)

:REMAP
echo -------------------------------
echo --                           --
echo --      Remapping W:\        --
echo --                           --
echo -------------------------------


subst W: /D
subst W: %cd%

:RUN
echo -------------------------------
echo --                           --
echo --      Starting SDK         --
echo --                           --
echo -------------------------------

FOR %%f IN (1) DO %SystemRoot%\system32\cmd.exe /c c:\Xilinx\SDK\%VIVADO_VER%\bin\xsdk -workspace w:\work\sdk

IF %ERRORLEVEL% == 1 (
   echo -----------------------------------------
   echo An ERROR Occured please check transcript!
   echo -----------------------------------------
)

:END